using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using VFSM.DAL.Models;

namespace VFSM.API.Models.AdminUserViewModels
{
    public class AdminUserUpdateViewModel
    {
        [Display(Name = "Id")]
        public string Id { get; set; }
        
        [Display(Name = "Please check roles you want to add.")]
        public List<ChceckboxModel> Roles { get; set; }
    }
}