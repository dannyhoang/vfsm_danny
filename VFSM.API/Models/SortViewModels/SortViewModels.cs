﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VFSM.API.Models
{
    public class SortViewModels
    {
        public string Sort { get; set; }
        private bool sa = true;
        public bool SortAsc { get {return sa;} set {sa = value;}}
    }
}
