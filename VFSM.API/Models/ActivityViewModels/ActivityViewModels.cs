﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VinaHR.DAL;

namespace VFSM.API.Models
{
    public class ActivityExportViewModels
    {
        public string DeviceId { get; set; }
        public string ActivityId { get; set; }
        public string EventType { get; set; }
        
        public int? Battery { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public bool  IsSeries { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
    public class ActivityViewModels
    {
        public string DeviceId { get; set; }
        public string Imei { get; set; }
        public string LastActivityId { get; set; }
        public string LastEventType { get; set; }
        
        public int? Battery { get; set; }
        public bool  HaveSos { get; set; }
        public bool  HaveFall { get; set; }
        public DateTime? ActivatedDate { get; set; }
    }
    public class ActivityResult : PagingParams
    {
        public List<ActivityViewModels> Items { get; set; }
    }
    public class ActivitySearchViewModels : SortViewModels
    {
        public string DeviceId { get; set; }
        public string UserId { get; set; }
        public string Imei { get; set; }
        public string Mac { get; set; }
        public string ActivityId { get; set; }
        public string EventType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    public class ActivityByDeviceViewModels
    {
        public string ActivityId { get; set; }
        public string DeviceId { get; set; }
        public string EventType { get; set; }
        public string LocSource { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public double? LatBeacon { get; set; }
        public double? LngBeacon { get; set; }
        public int? Battery { get; set; }
        public double? Accuracy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
