using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.API.Models.UserViewModels
{
    public class UserDeviceViewModel
    {
        [Column("CREATED_DATE")]
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }

        [Column("DEVICE_ID")]
        [Display(Name = "Device ID")]
        public string DeviceId { get; set; }

        [Column("DEVICE_UID")]
        [Display(Name = "Imei")]
        public string DeviceUid { get; set; }

        [Column("NAME")]
        [Display(Name = "Device Name")]
        public string Name { get; set; }

        [Column("IMEI")]
        [Display(Name = "Imei")]
        public string Imei { get; set; }

        [Column("ROLE")]
        [Display(Name = "Role")]
        public string Role { get; set; }

        [Column("STATUS")]
        [Display(Name = "Status")]
        public string Status { get; set; }

        [Column("USER_ID")]
        [Display(Name = "User Id")]
        public string UserId { get; set; }
    }
}