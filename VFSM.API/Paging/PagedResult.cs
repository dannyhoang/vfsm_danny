﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VFSM.API
{
    public class PagedResult<T> : PagingParams where T : class
    {
        public IEnumerable<T> Items { get; set; }
        public PagedResult()
        {
            Items = new List<T>();
        }
    }

    public static class PageResult
    {
        public static PagedResult<T> GetPaged<T>(IQueryable<T> query, int page, int pageSize, bool GetTotalItemOrNot) where T : class
        {
            var result = new PagedResult<T>();
            if (GetTotalItemOrNot)
            {
                result.TotalItems = query.Count();
            }

            if (page <= 0) page = 1;
            if (pageSize <= 0) pageSize = 10;

            result.CurrentPage = page;
            result.PageSize = pageSize;

            var pageCount = (double)result.TotalItems / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;

            if (query.Any())
                result.Items = query.Skip(skip).Take(pageSize);
            else
                result.Items = new List<T>();

            return result;
        }
    }
}
