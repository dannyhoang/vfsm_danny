﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VFSM.API
{
    public class PagingParams : IPagingParams
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }
        public int TotalItems { get; set; }

        //public int FristRownOnPage
        //{
        //    get { return (CurrentPage - 1) * PageSize + 1; }
        //}

        //public int LastRownOnPage
        //{
        //    get { return Math.Min(CurrentPage * PageSize, TotalItems); }
        //}
    }
}
