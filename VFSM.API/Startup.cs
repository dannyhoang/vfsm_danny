﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Amazon.Extensions.NETCore.Setup;
using Amazon.S3;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Amazon.Runtime;
using ReflectionIT.Mvc.Paging;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using VFSM.Services;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.DAL.Config;

namespace VFSM.API
{
    public class Startup
    {
        private ContainerBuilder containerBuilder;
        private IConfiguration _configuration;
        private IHostingEnvironment CurrentEnvironment { get; set; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add admin DB context
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseMySql(_configuration.GetConnectionString("AdminConnection")));

            // Add VF DB context
            services.AddDbContext<VodafoneContext>(options =>
                options.UseMySql(_configuration.GetConnectionString("VfebConnection")));

            services.AddIdentity<ApplicationUser, ApplicationRole>(config => { config.SignIn.RequireConfirmedEmail = false; })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            // Add JWT Authentication for API Clients
            services.AddAuthentication()
                .AddJwtBearer(option =>
                {
                    option.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                    {
                        // Clock skew compensates for server time drift. If you want to allow a certain amount of clock drift, set that here
                        ClockSkew = TimeSpan.Zero,
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        // Ensure the token hasn't expired:
                        RequireExpirationTime = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = _configuration["Jwt:JwtIssuer"],
                        ValidAudience = _configuration["Jwt:JwtAudience"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:SecretKey"])),
                        NameClaimType = ClaimTypes.NameIdentifier
                    };
                });

            services.Configure<IdentityOptions>(options => 
            {
                // Password settings
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 6;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings
                options.User.RequireUniqueEmail = true;
                
                // Email confirmed settings
                options.SignIn.RequireConfirmedEmail = true;
            });
            
            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                // If the LoginPath isn't set, ASP.NET Core defaults 
                // the path to /Account/Login.
                if (CurrentEnvironment.IsDevelopment()) 
                {
                    options.LoginPath = "/Account/Login";
                    options.AccessDeniedPath = "/Account/AccessDenied";
                }
                else 
                {
                    options.LoginPath = "/vfeb/v1/mgmt/Account/Login";
                    options.AccessDeniedPath = "/vfeb/v1/mgmt/Account/AccessDenied";
                }
                options.SlidingExpiration = true;
            });                        

            services.AddCors();

            services.AddMvc();
            services.AddPaging();

            // Add AWS S3 configuration
            var awsConfig = _configuration.GetSection("AWSConfig");
            services.Configure<AWSConfig>(awsConfig);

            // Add AWS service
            var awsOptions = _configuration.GetAWSOptions();
            awsOptions.Credentials = new BasicAWSCredentials(_configuration["AWSConfig:AWS_ACCESSS_KEY_ID"], _configuration["AWSConfig:AWS_SECRET_ACCESS_KEY"]);
            
            services.AddDefaultAWSOptions(awsOptions);
            services.AddAWSService<IAmazonS3>();
            services.AddAWSService<IAmazonSimpleEmailService>();

            // Add Vodafone API server URL configuration
            var vodafoneApiServerUrl = _configuration.GetSection("VodafoneConfig");
            services.Configure<VodafoneConfig>(vodafoneApiServerUrl);

            // Repositories. If not exist code bellow. Controller can't work because IUnitOfWork uninitialized
            services.AddScoped<IUnitOfWork, HttpUnitOfWork>();

            // Install DI services
            containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<ServiceInstaller>();
            containerBuilder.RegisterModule<DALInstaller>();
            containerBuilder.Populate(services);
            var container = containerBuilder.Build();
            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }


            //Configure Cors for API with METHOD POST/PUTH/DELETE...
            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod());

            app.UseStaticFiles();

            app.UseAuthentication();
            // Config router
            app.UseMvc();

            // 실행 환경에 따른 base path 설정
            if (env.IsDevelopment())
            {
                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}");
                });
            }
            else
            {
                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "vfeb/v1/mgmt/{controller=Home}/{action=Index}/{id?}");
                });
                // HTTP to HTTPS redirect setting
                var options = new RewriteOptions().AddRedirectToHttps();
                app.UseRewriter(options);
            }
            CurrentEnvironment = env;       
        }
    }
}
