using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL;
using VFSM.DAL.Models;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Routing;
using VFSM.API.Models.RoleViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using VFSM.API.Models;

namespace VFSM.API.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [Produces("application/json")]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RoleController : BaseController
    {
        private RoleManager<ApplicationRole> _roleManager;

        public RoleController(VodafoneContext vfContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager,
           RoleManager<ApplicationRole> roleManager)
           : base(vfContext, context, unitOfWork, userManager)
        {
            _roleManager = roleManager;
        }
        
        #region Search
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> Search([FromQuery] PagingViewModel page, [FromQuery] RoleSearchViewModel modelSearch)
        {
            var roles = await _roleManager.Roles.ToListAsync();
            IList<RoleViewModel> roleViewModels = new List<RoleViewModel>();
            foreach (var role in roles.OrderBy(r => r.Name))
            {
                roleViewModels.Add(new RoleViewModel { Id = role.Id, Name = role.Name });
            }

            return Ok(roleViewModels);
        }
        #endregion
        [HttpGet]
        [Route("[action]/{id}")]
        public async Task<IActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("ID must not be null or empty.");
            }

            var role = await _roleManager.Roles.SingleOrDefaultAsync(r => r.Id == id);
            if (role == null)
            {
                return BadRequest();
            }

            var userInRole = await _userManager.GetUsersInRoleAsync(role.Name);
            var listPageWithOperationByRole = (from p in _appDbContext.VodafonePages
                                                where p.Active == true
                                                join r in _appDbContext.VodafonePageRoles.Where(o => o.RoleID == id) on p.Id equals r.PageID into rGroup
                                                from rr in rGroup.DefaultIfEmpty()
                                                select new PageViewModel {
                                                    Id = p.Id,
                                                    Name = p.Name,
                                                    Value = p.Value,
                                                    Order = p.Order,
                                                    Parent_ID = p.ParentId,
                                                    OperationID = rr.OperationID != null ? rr.OperationID : ""
                                                }).ToList();
            var result = new RolesViewMode { 
                Id = role.Id, 
                Name = role.Name, 
                Description = role.Description,
                Pages = listPageWithOperationByRole
            };
            return Ok(result);
        }

        #region Create policy

        [HttpPost]
        [Route("[action]")] 
        public async Task<IActionResult> Create([FromBody]RolesViewMode model)
        {
            if (ModelState.IsValid)
            {
                var resp = new ResponseViewModel();
                var roleExist = await _roleManager.Roles.SingleOrDefaultAsync(r => r.Name == model.Name);
                if (roleExist != null)
                {
                    resp.Status = "existed";
                    return Ok(resp);
                }
                var role = new ApplicationRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = model.Name,
                    Description = model.Description
                };

                var pagesRole = new List<VodafonePageRole>();
                var pageOperations = new List<VodafonePageOperation>();

                // Add page for each role
                foreach (var item in model.Pages)
                {
                    var pageRole = new VodafonePageRole
                    {
                        RoleID = role.Id,
                        PageID = item.Id,
                        OperationID = item.OperationID
                    };
                    pagesRole.Add(pageRole);

                    // Add operation Create/Update/Delete/View... for pages
                    //foreach (var operation in item.Operations)
                    //{
                    //    var pageOperation = new VodafonePageOperation
                    //    {
                    //        PageID = item.Id,
                    //        OperationID = operation.Id
                    //    };
                    //    pageOperations.Add(pageOperation);
                    //}
                }

                using (var scope = _appDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        _vodaUnitOfWork.Role.Add(role);
                        _vodaUnitOfWork.VodafonePageRole.AddRange(pagesRole);
                        //_vodaUnitOfWork.VodafonePageOperation.AddRange(pageOperations);

                        _vodaUnitOfWork.SaveChange();

                        scope.Commit();

                        resp.Status = "Success";
                        resp.Data = role;

                        return Ok(resp);
                    }
                    catch (Exception ex)
                    {
                        scope.Rollback();
                        return BadRequest(ex.Message);
                    }
                }
                //        // var result = await _roleManager.CreateAsync(new ApplicationRole(role.RoleName));
                //        if (result.Succeeded)
                //{
                //    return Ok(role);
                //}
                //return BadRequest();
            }
            return BadRequest(ModelState);
        }
        #endregion
        #region Edit role
        [HttpPut]
        [Route("[action]/{id}")]
        public async Task<IActionResult> Edit(string id, [FromBody]RolesViewMode model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }

            if (ModelState.IsValid)
            {
                var resp = new ResponseViewModel();
                var role = await _roleManager.FindByIdAsync(model.Id);
                if (role.Name == "Admin")
                {
                    return BadRequest($"Unable to edit role with ID '{id}'.");
                }
                role.Name = model.Name;
                role.Description = model.Description;
                var listRolePage = new List<VodafonePageRole>();
                var listOldPageRole = _appDbContext.VodafonePageRoles.Where(o => o.RoleID == model.Id).ToList();
                foreach (var item in model.Pages)
                {
                    if (item.OperationID != null && item.OperationID != "")
                    {
                        var itemRolePage = new VodafonePageRole {
                            PageID = item.Id,
                            RoleID = role.Id,
                            OperationID = item.OperationID
                        };
                        listRolePage.Add(itemRolePage);
                    }
                }
                using (var scope = _appDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        
                        await _roleManager.UpdateAsync(role);
                        if (listOldPageRole != null)
                        {
                            _vodaUnitOfWork.VodafonePageRole.RemoveRange(listOldPageRole);
                        }
                        if (listRolePage != null)
                        {
                            _vodaUnitOfWork.VodafonePageRole.AddRange(listRolePage);  
                        }
                        //_vodaUnitOfWork.VodafonePageOperation.AddRange(pageOperations);

                        _vodaUnitOfWork.SaveChange();

                        scope.Commit();

                        resp.Status = "Success";
                        resp.Data = role;

                        return Ok(resp);
                    }
                    catch (Exception ex)
                    {
                        scope.Rollback();
                        return BadRequest(ex.Message);
                    }
                }
            }
            return BadRequest("Data not valid!");
        }
            
        #endregion


        [HttpDelete]
        [Route("[action]/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var role = await _roleManager.Roles.SingleOrDefaultAsync(r => r.Id == id);
            if (role.Name == "Admin")
            {
                throw new ApplicationException($"Unable to delete role with ID '{id}'.");
            }

            var result = await _roleManager.DeleteAsync(role);
            if (result.Succeeded)
            {
                return Ok("success");
            }
            else
            {
                throw new ApplicationException(result.Errors.FirstOrDefault().Description);
            }
        }
    }
}

