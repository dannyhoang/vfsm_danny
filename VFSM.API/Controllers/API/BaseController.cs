﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VFSM.DAL;
using VFSM.DAL.Models;

namespace VFSM.API.Controllers.API
{
    public class BaseController : Controller
    {
        private readonly VodafoneContext _vodaContext;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWork _unitOfWork;
        protected UserManager<ApplicationUser> _userManager;

        public BaseController(VodafoneContext vodaContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager)
        {
            _vodaContext = vodaContext;
            _context = context;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public VodafoneContext _vfDbContext => (VodafoneContext)_vodaContext;
        public ApplicationDbContext _appDbContext => (ApplicationDbContext)_context;

        public IUnitOfWork _vodaUnitOfWork => (IUnitOfWork)_unitOfWork;

        public UserManager<ApplicationUser> _vodaUserManager => (UserManager<ApplicationUser>)_userManager;

        #region Exists methods
        protected bool VodafoneModelExists(string id)
        {
            return _vfDbContext.VodafoneModels.Any(e => e.ModelId == id);
        }

        protected bool VodafoneDeviceExists(string id)
        {
            return _vfDbContext.VodafoneDevices.Any(e => e.DeviceId == id);
        }

        protected bool VodafoneOTAExists(string modelId, string category, string version)
        {
            return _vfDbContext.VodafoneOtas.Any(o => o.ModelId == modelId && o.Category == category && o.Version == version);
        }

        protected bool VodafoneOTAMappingVersionExists(string mappingVersion)
        {
            return _vfDbContext.VodafoneOtas.Any(o => o.MappingVersion == mappingVersion);
        }

        protected bool VodafoneAdminUserExists(string id)
        {
            return _userManager.Users.Any(u => u.Id == id);
        }
        #endregion

        /// <summary>
        /// Get object user current login by info token
        /// Check by username or email 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public async Task<ApplicationUser> GetUserAsync()
        {
            // Get info from token
            var user = await _vodaUserManager.FindByNameAsync(_appDbContext.CurrentUserName);

            if (user != null)
                return user;
            return null;
        }

        public void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}