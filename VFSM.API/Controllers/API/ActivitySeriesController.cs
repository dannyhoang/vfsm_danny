using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.API.Constants;
using VFSM.DAL.Models.DBModels;
using VFSM.API.Models.UserViewModels;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using VFSM.API.Models;
using VFSM.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace VFSM.API.Controllers.API
{
    //[Authorize(Roles = "Admin, CS Read, CS Write")]
    [Produces("application/json")]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ActivitySeriesController : BaseController
    {

        public ActivitySeriesController(VodafoneContext vfContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager)
           : base(vfContext, context, unitOfWork, userManager)
        {
        }

        #region Detail
        [HttpGet]
        [Route("[action]/{actId}")]
        public IActionResult GetByActId(string actId)
        {
            if (string.IsNullOrEmpty(actId))
            {
                return BadRequest();
            }
            try
            {
                var query = from acts in _vfDbContext.VodafoneActivitySeries
                            where acts.ActId == actId
                            orderby acts.CreatedDate descending
                            select acts;
                return Ok(query.ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}