using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.API.Constants;
using VFSM.DAL.Models.DBModels;
using VFSM.API.Models.UserViewModels;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using VFSM.API.Models;
using VFSM.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace VFSM.API.Controllers.API
{
    //[Authorize(Roles = "Admin, CS Read, CS Write")]
    [Produces("application/json")]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : BaseController
    {
        private readonly IExcelFileService _excelFileService;

        public UserController(VodafoneContext vfContext,
        ApplicationDbContext context,
        IUnitOfWork unitOfWork,
        UserManager<ApplicationUser> userManager,
        IExcelFileService excelFileService)
        : base(vfContext, context, unitOfWork, userManager)
        {
            _excelFileService = excelFileService;
        }

        #region Search
        [HttpGet]
        public IActionResult Search([FromQuery] PagingViewModel page, [FromQuery] UserSearchViewModel modelSearch)
        {
            if (ModelState.IsValid)
            {
                var a = modelSearch.FromDate.Day;
                var b = modelSearch.FromDate.Month;
                IQueryable<UserViewModel> qry = null;
                //Imei = _vfDbContext.VodafoneDevices.Where(d => d.DeviceId == _vfDbContext.VodafoneUserDevices.Where(x => x.UserId == u.UserId).FirstOrDefault().DeviceId).FirstOrDefault().DeviceUid,
                qry = _vfDbContext.VodafoneUsers.Select(u => new UserViewModel
                {
                    CreatedDate = u.CreatedDate,
                    UserId = u.UserId,
                    NickName = u.NickName,
                    MobileType = u.MobileType,
                    Imei = u.Imei
                });
                if (modelSearch.Mac != null)
                {
                    var beacon = _vfDbContext.VodafoneBeacons.Where(x => x.Mac.Contains(modelSearch.Mac));
                    var device = _vfDbContext.VodafoneUserDevices.Where(x => beacon.Any(y => y.DeviceId == x.DeviceId));
                    qry = qry.Where(x => device.Any(y => y.UserId == x.UserId));
                }
                if (modelSearch.UserId != null)
                {
                    qry = qry.Where(x => x.UserId.Contains(modelSearch.UserId));
                }
                if (modelSearch.NickName != null)
                {
                    qry = qry.Where(x => x.NickName.Contains(modelSearch.NickName));
                }
                if (modelSearch.Imei != null)
                {
                    var device = _vfDbContext.VodafoneDevices.Where(x => x.DeviceUid.Contains(modelSearch.Imei));
                    var udevice = _vfDbContext.VodafoneUserDevices.Where(x => device.Any(y => y.DeviceId == x.DeviceId));
                    qry = qry.Where(x => udevice.Any(y => y.UserId == x.UserId));
                }
                if (modelSearch.FromDate != default(DateTime))
                {
                    qry = qry.Where(x => x.CreatedDate.Date >= new DateTime(modelSearch.FromDate.Year, modelSearch.FromDate.Month, modelSearch.FromDate.Day, 0, 0, 0));
                }
                if (modelSearch.ToDate != default(DateTime))
                {
                    qry = qry.Where(x => x.CreatedDate.Date <= new DateTime(modelSearch.ToDate.Year, modelSearch.ToDate.Month, modelSearch.ToDate.Day, 23, 59, 59));
                }
                switch (modelSearch.Sort)
                {
                    case SortUser.CreatedDate:
                        qry = modelSearch.SortAsc == true ? qry.OrderBy(o => o.CreatedDate) : qry.OrderByDescending(o => o.CreatedDate);
                        break;
                    case SortUser.UserId:
                        qry = modelSearch.SortAsc == true ? qry.OrderBy(o => o.UserId) : qry.OrderByDescending(o => o.UserId);
                        break;
                    case SortUser.NickName:
                        qry = modelSearch.SortAsc == true ? qry.OrderBy(o => o.NickName) : qry.OrderByDescending(o => o.NickName);
                        break;
                    case SortUser.MobileType:
                        qry = modelSearch.SortAsc == true ? qry.OrderBy(o => o.MobileType) : qry.OrderByDescending(o => o.MobileType);
                        break;
                    case SortUser.Imei:
                        qry = modelSearch.SortAsc == true ? qry.OrderBy(o => o.Imei) : qry.OrderByDescending(o => o.Imei);
                        break;
                    default:
                        qry = qry.OrderByDescending(o => o.CreatedDate);
                        break;
                }
                var pages = PageResult.GetPaged(qry, page.CurrentPage, page.PageSize, PagingSupport.GetTotal);
                pages.Items = pages.Items.ToList();
                var listUserId = new List<string>();
                foreach (var item in pages.Items)
                {
                    listUserId.Add(item.UserId);

                }
                var ud = (from udqry in _vfDbContext.VodafoneUserDevices.Where(x => listUserId.Contains(x.UserId))
                          join dd in _vfDbContext.VodafoneDevices on udqry.DeviceId equals dd.DeviceId into ds
                            from d in ds.DefaultIfEmpty()
                          join beaconn in _vfDbContext.VodafoneBeacons on d.DeviceId equals beaconn.DeviceId into bs
                            from beacon in bs.DefaultIfEmpty()
                          select new DeviceOfUserViewModel
                          {
                              DeviceId = d.DeviceId,
                              DeviceVersion = d.DeviceVersion,
                              BeaconMac = beacon.Mac,
                              ActivatedDate = d.ActivatedDate,
                              Status = d.Status,
                              Imei = d.DeviceUid,
                              DeviceName = d.Name,
                              LatBeacon = beacon.Lat,
                              LngBeacon = beacon.Lng,
                              Role = udqry.Role,
                              UserId = udqry.UserId
                          }).ToList();
                foreach (var item in pages.Items)
                {
                    item.Device = ud.Where(x => x.UserId == item.UserId);
                }
                return Ok(pages);
            }
            return BadRequest("Data not valid.");
        }
        #endregion

        #region Detail
        [HttpGet("{userId}")]
        public IActionResult ActivityList(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return BadRequest();
            }

            var model = from u in _vfDbContext.VodafoneUsers.Where(x => x.UserId == userId)
                        select new
                        {
                            u.CreatedDate,
                            u.UserId,
                            u.NickName,
                            u.MobileType,
                            Imei = _vfDbContext.VodafoneDevices.Where(d => d.DeviceId == _vfDbContext.VodafoneUserDevices.Where(x => x.UserId == u.UserId).FirstOrDefault().DeviceId).FirstOrDefault().DeviceUid,
                            RootCount = _vfDbContext.VodafoneUserDevices.Where(x => x.UserId == u.UserId && x.Role == "ROOT").Count(),
                            SubCount = _vfDbContext.VodafoneUserDevices.Where(x => x.UserId == u.UserId && x.Role == "SUB").Count(),
                            AppVersion = u.VersionCode
                        };
            return Ok(model.FirstOrDefault());
        }
        #endregion

        #region DeviceList
        [HttpGet]
        [Route("device")]
        public async Task<IActionResult> DeviceList([FromQuery]string userId, int page = 1)
        {
            if (string.IsNullOrEmpty(userId))
            {
                return BadRequest();
            }
            var model = from ud in _vfDbContext.VodafoneUserDevices.Where(x => x.UserId == userId)
                        join d in _vfDbContext.VodafoneDevices on ud.DeviceId equals d.DeviceId
                        select new UserDeviceViewModel
                        {
                            CreatedDate = d.CreatedDate,
                            DeviceId = d.DeviceId,
                            DeviceUid = d.DeviceUid,
                            Name = d.Name,
                            Imei = d.DeviceUid,
                            Role = ud.Role,
                            Status = d.Status.Replace("\"", "").Replace("[", "").Replace("]", ""),
                            UserId = userId
                        };

            var result = await PaginatedList<UserDeviceViewModel>.CreateAsync(model.AsNoTracking(), page, VF_CONSTANT.PageSize);
            return Ok(result);
        }
        #endregion

        #region List Activity of device
        [HttpGet]
        [Route("activity")]
        public async Task<IActionResult> ActivityList([FromQuery]string deviceId, int page = 1)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                return BadRequest();
            }

            var model = _vfDbContext.VodafoneActivities.Where(x => x.DeviceId == deviceId).OrderByDescending(x => x.CreatedDate);
            var result = await PaginatedList<VodafoneActivity>.CreateAsync(model.AsNoTracking(), page, VF_CONSTANT.PageSize);
            return Ok(result);
        }
        #endregion

        #region Activity Series List
        [HttpGet]
        [Route("activity-series")]
        public async Task<IActionResult> ActivitySeriesList([FromQuery]string id, int page = 1)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }

            var model = _vfDbContext.VodafoneActivitySeries.Where(x => x.ActId == id).OrderByDescending(x => x.CreatedDate);
            var result = await PaginatedList<VodafoneActivitySeries>.CreateAsync(model.AsNoTracking(), page, VF_CONSTANT.PageSize);
            return Ok(result);
        }
        #endregion

        #region Register Device
        [HttpPost]
        [Route("device")]
        public async Task<IActionResult> Register([FromBody]DeviceRegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _vfDbContext.VodafoneUsers.Where(u => u.UserId == model.UserId).FirstOrDefault();
                var device = await _vfDbContext.VodafoneDevices.Where(d => d.DeviceUid == model.DeviceUid).FirstOrDefaultAsync();

                try
                {
                    var generatedDeviceId = Guid.NewGuid().ToString();
                    var userdevice = new VodafoneUserDevice
                    {
                        UserId = model.UserId,
                        DeviceId = generatedDeviceId,
                        RegName = model.DeviceName,
                        Role = model.Role,
                        CreatedDate = DateTime.UtcNow
                    };
                    if (device == null)
                    {
                        device = new VodafoneDevice()
                        {
                            DeviceVersion = "0.0.0.0",
                            DeviceId = generatedDeviceId,
                            ImgUrl = string.Empty,
                            DeviceUid = model.DeviceUid,
                            Name = model.DeviceName,
                            ActivatedDate = DateTime.UtcNow,
                            Activated = true,
                            ModelId = "31-00006",
                            Status = "[\"OK\"]",
                            StatusLevel = "NORMAL",
                            CreatedDate = DateTime.UtcNow
                        };

                        await _vfDbContext.VodafoneDevices.AddAsync(device);

                        await _vfDbContext.VodafoneUserDevices.AddAsync(userdevice);
                        _vfDbContext.SaveChanges();
                    }
                    else
                    {
                        // 등록유저가 있을 때 - UserDevice에 SUB로 등록
                        await _vfDbContext.VodafoneUserDevices.AddAsync(userdevice);
                        _vfDbContext.SaveChanges();
                    }

                    var response = new DeviceResponseViewModel
                    {
                        UserId = userdevice.UserId,
                        DeviceId = userdevice.DeviceId,
                        DeviceName = userdevice.RegName,
                        Role = userdevice.Role,
                        DeviceVersion = device.DeviceVersion,
                        Imei = device.DeviceUid,
                        Battery = device.Battery,
                        ActivatedDate = device.ActivatedDate,
                        Activated = device.Activated,
                        ModelId = device.ModelId,
                        Status = device.Status
                    };
                    return Ok(response);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Paramater is not vaild.");
        }
        #endregion

        #region UserNickNameModify
        [HttpPut]
        [Route("nick-name")]
        public async Task<IActionResult> UserNickNameModify([FromBody]UserNickNameModifyViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.UserId == null)
                    return BadRequest();

                try
                {
                    var user = await _vfDbContext.VodafoneUsers.Where(u => u.UserId == model.UserId).FirstOrDefaultAsync();
                    if (user == null)
                    {
                        return BadRequest("User not found. check your USER ID.");
                    }
                    user.NickName = model.NickName;
                    user.ModifiedDate = DateTime.UtcNow;
                    _vfDbContext.VodafoneUsers.Update(user);
                    _vfDbContext.SaveChanges();
                    return Ok(new UserNickNameModifyViewModel
                    {
                        UserId = user.UserId,
                        NickName = user.NickName
                    });
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Paramater is not vaild.");
        }
        #endregion

        #region DeviceNameModify
        [HttpPut]
        [Route("device")]
        public async Task<IActionResult> DeviceNameModify([FromBody]DeviceNameModifyViewModel model)
        {
            var result = new ResponseModel { Success = false };
            if (ModelState.IsValid)
            {
                var device = await _vfDbContext.VodafoneDevices.Where(d => d.DeviceId == model.DeviceId).FirstOrDefaultAsync();
                if (device == null)
                {
                    result.Message = "Deivce is not found. Check your DEVICE ID.";
                }
                else
                {
                    device.Name = model.Name;
                    device.ModifiedDate = DateTime.UtcNow;
                    _vodaUnitOfWork.VodafoneDevices.Update(device);
                    result.Success = true;
                    result.Message = "Deivce Name Modify is success.";
                    result.RedirectMethod = "deviceList";
                    result.ParameterId = model.UserId;
                    result.ParameterPage = model.Page;
                }
            }
            else
            {
                result.Message = "Paramater is not vaild.";
            }
            return Ok(result);
        }
        #endregion

        #region SendMessage
        [HttpPost]
        [Route("send-message")]
        public async Task<IActionResult> SendMessage([FromBody]SendMessageViewModel model)
        {
            var result = new ResponseModel { Success = false };
            if (ModelState.IsValid)
            {
                //using (HttpResponseMessage response = await _httpClient.GetAsync(
                //   string.Format("message?deviceId={0}&lat={1}&lng={2}&battery={3}&msgType={4}", model.DeviceUid, model.Lat, model.Lng, model.Battery,model.MessageType)))
                //{
                //    if (response.IsSuccessStatusCode)
                //    {
                //        var responseResult = JsonConvert.DeserializeObject<ResponseModel>(await response.Content.ReadAsStringAsync());
                //        if (responseResult.Success)
                //        {
                //           result.Success = true;
                //           result.Message = "Message sent complete.";
                //           result.RedirectMethod = "deviceList";
                //           result.ParameterId = model.UserId;
                //           result.ParameterPage = model.Page;
                //        }
                //   }
                //}
            }
            return Ok(result);
        }
        #endregion

        #region DeleteUserDevice
        [HttpDelete]
        [Route("device")]
        public async Task<IActionResult> DeleteUserDevice([FromBody]DeleteUserDeviceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userDevice = await _vfDbContext.VodafoneUserDevices.Where(ud => ud.UserId == model.UserId && ud.DeviceId == model.DeviceId).FirstOrDefaultAsync();
                if (userDevice == null)
                {
                    return BadRequest("User Device not found. Check your USER ID OR DEVICE ID.");
                }
                else
                {
                    _vfDbContext.VodafoneUserDevices.Remove(userDevice);
                    _vfDbContext.SaveChanges();
                    return Ok("Remove success.");
                }
            }
            return BadRequest("Data not valid.");
        }
        #endregion

        #region excel
        [HttpGet]
        [Route("[action]")]
        public IActionResult Export([FromQuery] UserSearchForExportViewModel modelSearch)
        {
            IQueryable<UserShowViewModel> qry = null;
            //Imei = _vfDbContext.VodafoneDevices.Where(d => d.DeviceId == _vfDbContext.VodafoneUserDevices.Where(x => x.UserId == u.UserId).FirstOrDefault().DeviceId).FirstOrDefault().DeviceUid,
            qry = _vfDbContext.VodafoneUsers.Select(u => new UserShowViewModel
            {
                CreatedDate = u.CreatedDate,
                UserId = u.UserId,
                NickName = u.NickName,
                MobileType = u.MobileType,
                Imei = u.Imei,
                DeviceCount = _vfDbContext.VodafoneUserDevices.Where(x => x.UserId == u.UserId).Count()
            });
            if (modelSearch.UserId != null)
            {
                qry = qry.Where(x => x.UserId.Contains(modelSearch.UserId));
            }
            if (modelSearch.Imei != null)
            {
                var device = _vfDbContext.VodafoneDevices.Where(x => x.DeviceUid.Contains(modelSearch.Imei));
                var udevice = _vfDbContext.VodafoneUserDevices.Where(x => device.Any(y => y.DeviceId == x.DeviceId));
                qry = qry.Where(x => udevice.Any(y => y.UserId == x.UserId));
                // qry = qry.Where(x => x.Imei.Contains(modelSearch.Imei));
            }
            var userList = new List<dynamic>();
            userList.Add(qry.FirstOrDefault());
            var dataModel = new ExportFileViewModel
            {
                Data = userList,
                HeaderRow = 1,
                DataStartRow = 2
            };
            var listDataModel = new List<ExportFileViewModel>();
            listDataModel.Add(dataModel);
            var listUserId = new List<string>();
            foreach (var item in userList)
            {
                listUserId.Add(item.UserId);
            }
            // list Device
            IQueryable<DeviceExportListViewModel> qryDevice = null;
            qryDevice = from ud in _vfDbContext.VodafoneUserDevices
                        where listUserId.Contains(ud.UserId)
                        join d in _vfDbContext.VodafoneDevices on ud.DeviceId equals d.DeviceId
                        select new DeviceExportListViewModel
                        {
                            DeviceId = ud.DeviceId,
                            CreatedDate = d.CreatedDate,
                            CustomerId = ud.UserId,
                            Name = d.Name,
                            Imei = d.DeviceUid,
                            Role = ud.Role,
                            Status = d.Status,
                            Version = d.DeviceVersion
                        };
            dataModel = new ExportFileViewModel
            {
                Data = qryDevice.ToList(),
                HeaderRow = 1,
                DataStartRow = 2
            };
            listDataModel.Add(dataModel);

            // list activity
            var listDeviceId = new List<string>();
            foreach (var item in qryDevice.ToList())
            {
                listDeviceId.Add(item.DeviceId);
            }
            IQueryable<ActivityExportViewModels> qryActivity = null;
            qryActivity = from a in _vfDbContext.VodafoneActivities
                          where listDeviceId.Contains(a.DeviceId)
                          select new ActivityExportViewModels
                          {
                              DeviceId = a.DeviceId,
                              CreatedDate = a.CreatedDate,
                              ActivityId = a.ActivityId,
                              EventType = a.EventType,
                              Battery = a.Battery,
                              Lat = a.Lat,
                              Lng = a.Lng,
                              IsSeries = a.IsSeries
                          };
            dataModel = new ExportFileViewModel
            {
                Data = qryActivity.ToList(),
                HeaderRow = 1,
                DataStartRow = 3
            };
            listDataModel.Add(dataModel);
            // List Activity History
            var listActId = new List<string>();
            foreach (var item in qryActivity.ToList())
            {
                if (item.EventType == "SOS" || item.EventType == "FALL")
                {
                    listActId.Add(item.ActivityId);
                }
            }
            IQueryable<ActivityServiesExportViewModels> qryActivitySeries = null;
            qryActivitySeries = from ase in _vfDbContext.VodafoneActivitySeries
                                where listActId.Contains(ase.ActId)
                                join act in _vfDbContext.VodafoneActivities on ase.ActId equals act.ActivityId
                                select new ActivityServiesExportViewModels
                                {
                                    ActSeriesSeq = ase.ActSeriesSeq,
                                    CreatedDate = ase.CreatedDate,
                                    DeviceId = act.DeviceId,
                                    EventType = ase.Action,
                                    ActId = ase.ActId,
                                    Lat = ase.Lat,
                                    Lng = ase.Lng,
                                    Message = ase.Message
                                };
            dataModel = new ExportFileViewModel
            {
                Data = qryActivitySeries.ToList(),
                HeaderRow = 1,
                DataStartRow = 3
            };
            listDataModel.Add(dataModel);
            var file = _excelFileService.FillDataIntoTemplateMultiSheet(listDataModel, "user-device-action-history-list");
            /*base64String = Convert.ToBase64String(imageBytes);*/
            if (file != null)
                return Ok(new { File = file, FileName = "user-device-action-history-list" });
            return new NoContentResult();
        }
        #endregion
    }
}
