using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.API.Constants;
using VFSM.DAL.Models.DBModels;
using VFSM.API.Models.UserViewModels;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace VFSM.API.Controllers.API
{
    //[Authorize(Roles = "Admin, CS Read, CS Write")]
    [Produces("application/json")]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BeaconController : BaseController
    {

        public BeaconController(VodafoneContext vfContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager)
           : base(vfContext, context, unitOfWork, userManager)
        {
        }

        #region Get by mac
        [HttpGet("{mac}")]
        public IActionResult GetByMac(string mac)
        {
            if (string.IsNullOrEmpty(mac))
            {
                return BadRequest();
            }
            else
            {
                var beacon = _vfDbContext.VodafoneBeacons.Where(b => b.Mac == mac).FirstOrDefault();
                return Ok(beacon);
            }
        }
        #endregion
    }
}