﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VFSM.DAL;
using VFSM.DAL.Models;

namespace VFSM.API.Controllers.API
{
    [Produces("application/json")]
    [Route("[controller]")]
    public class VodafoneUserController : BaseController
    {
        private readonly SignInManager<ApplicationUser> _signManager;
        private readonly ILogger _logger;

        public VodafoneUserController(VodafoneContext vfContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signManager,
           ILogger<AccountController> logger)
           : base(vfContext, context, unitOfWork, userManager)
        {
            _signManager = signManager;
            _logger = logger;
        }

        /// <summary>
        /// Get all vacation
        /// </summary>
        [HttpGet]
        [Route("~/voda-users")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> GetAll()
        {
            var user = await GetUserAsync();
            if (user == null)
                return Unauthorized();

            var list = _vodaUnitOfWork.VodafoneUsers.GetAll();
            if (list.Any())
                return Ok(list);
            return Ok(new List<object>());
        }
    }
}