using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.API.Constants;
using VFSM.DAL.Models.DBModels;
using VFSM.API.Models.UserViewModels;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using VFSM.API.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace VFSM.API.Controllers.API
{
    //[Authorize(Roles = "Admin, CS Read, CS Write")]
    [Produces("application/json")]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ModelController : BaseController
    {

        public ModelController(VodafoneContext vfContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager)
           : base(vfContext, context, unitOfWork, userManager)
        {
        }

        #region Search
        // GET: VodafoneModel 
        [HttpGet]
        [Route("[action]")]
        public async Task<IActionResult> Search([FromQuery] PagingViewModel page, [FromQuery] ModelSearchViewModel modelSearch)
        {
            IQueryable<VodafoneModel> qry = null;
            qry = _vfDbContext.VodafoneModels.Where(m => m.DelFlag == false)
                .Select(m => new VodafoneModel
            {
                ModelId = m.ModelId,
                Name = m.Name,
                CreateDate = m.CreateDate,
                Description = m.Description,
                ModifiedDate = m.ModifiedDate,
                DelFlag = m.DelFlag
            });
            if (modelSearch.ModelId != null)
            {
                qry = qry.Where(x => x.ModelId.Contains(modelSearch.ModelId) || x.Name.Contains(modelSearch.ModelId));
            }
            if (modelSearch.FromDate != default(DateTime))
            {
                qry = qry.Where(x => x.CreateDate > new DateTime(modelSearch.FromDate.Year, modelSearch.FromDate.Month, modelSearch.FromDate.Day, 0, 0, 0));
            }
            if (modelSearch.ToDate != default(DateTime))
            {
                qry = qry.Where(x => x.CreateDate < new DateTime(modelSearch.ToDate.Year, modelSearch.ToDate.Month, modelSearch.ToDate.Day, 23, 59, 59));
            }
            switch (modelSearch.Sort)
            {
                case SortModel.CreatedDate:
                    qry = modelSearch.SortAsc == true? qry.OrderBy(o => o.CreateDate): qry.OrderByDescending(o => o.CreateDate);
                    break;
                case SortModel.ModelId:
                    qry = modelSearch.SortAsc == true? qry.OrderBy(o => o.ModelId): qry.OrderByDescending(o => o.ModelId);
                    break;
                case SortModel.Name:
                    qry = modelSearch.SortAsc == true? qry.OrderBy(o => o.Name): qry.OrderByDescending(o => o.Name);
                    break;
                case SortModel.ModifiedDate:
                    qry = modelSearch.SortAsc == true? qry.OrderBy(o => o.ModifiedDate): qry.OrderByDescending(o => o.ModifiedDate);
                    break;
                default:
                    qry = modelSearch.SortAsc == true? qry.OrderBy(o => o.CreateDate): qry.OrderByDescending(o => o.CreateDate);
                    break;
            }
            var pages = PageResult.GetPaged(qry, page.CurrentPage, page.PageSize, PagingSupport.DontGetTotal);
            return Ok(pages);
        }
        #endregion

        #region Details
        // GET: VodafoneModel/Details/5
        [HttpGet("{id}")]
        //[Route("[action]")]
        // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest();
            }

            var vodafoneModel = from m in _vfDbContext.VodafoneModels
                                where m.ModelId == id
                                select new ModelDetailViewModel {
                                    ModelId = m.ModelId,
                                    Name = m.Name,
                                    Description = m.Description,
                                    CreatedDate = m.CreateDate,
                                    ModifiedDate = m.ModifiedDate,
                                    DelFlag = m.DelFlag
                                };
            var modelOta = from ota in _vfDbContext.VodafoneOtas
                                where ota.ModelId == id
                                orderby ota.CreatedDate descending
                                select new OTAViewNotFileModel{
                                    ModelId = ota.ModelId,
                                    Version = ota.Version,
                                    Category = ota.Category,
                                    IsActive = ota.IsActive,
                                    CreatedDate = ota.CreatedDate,
                                    ModifiedDate = ota.ModifiedDate,
                                };
            var listMC60 = new List<OTAViewNotFileModel>();
            var listFirmware = new List<OTAViewNotFileModel>();
            var res = vodafoneModel.FirstOrDefault();
            if (res == null)
            {
                return BadRequest("Model not exist.");
            }
            var listModelOta = modelOta.ToList();
            List<DateTime?> listDateTime = new List<DateTime?>();
            listDateTime.Add(res.CreatedDate);     
            listDateTime.Add(res.ModifiedDate);  
            
            if (listModelOta.Count() > 0)
            {   
                listDateTime.Add(listModelOta[0].CreatedDate);     
                listDateTime.Add(listModelOta[0].ModifiedDate); 
            }
            // last update date
            res.LastUpdateDate = listDateTime.Max(p => p);
            foreach (var item in modelOta)
            {
                if (item.Category == "MC60")
                {
                    listMC60.Add(item);
                }
                if (item.Category == "FIRMWARE")
                {
                    listFirmware.Add(item);
                }
            }
            if (listMC60.Count() > 0)
            {
                res.VersionMC60 = listMC60[0].Version;
            }
            if (listMC60.Count() > 0)
            {
                res.VersionFirmware = listFirmware[0].Version;
            }
            return Ok(res);
        }
        #endregion

        #region Create
        // POST: VodafoneModel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public IActionResult Create([FromBody] VodafoneModel vodafoneModel)
        {
            var resp = new ResponseViewModel();
            if (ModelState.IsValid)
            {
                var model = _vfDbContext.VodafoneModels.Where(m => m.ModelId == vodafoneModel.ModelId).FirstOrDefault();
                if (model != null)
                {
                    if(model.DelFlag == false)
                    {
                        resp.Status = "ModelId existed.";
                        resp.Data = vodafoneModel;
                        return Ok(resp);
                    }
                    else
                    {
                        _vfDbContext.VodafoneModels.Remove(model);
                    }
                }
                vodafoneModel.CreateDate = DateTime.Now;
                vodafoneModel.DelFlag = false;
                _vfDbContext.VodafoneModels.Add(vodafoneModel);
                _vfDbContext.SaveChanges();
                resp.Status = "Success";
                resp.Data = vodafoneModel;
                return Ok(resp);
            }
            return BadRequest("Data not valid");
        }
        #endregion


        #region Update
        [HttpPut]
        public IActionResult Edit([FromBody] ModelViewModel vodafoneModel)
        {
            var resp = new ResponseViewModel();
            if (string.IsNullOrEmpty(vodafoneModel.ModelId))
            {
                resp.Status = "ModelId can't null.";
                resp.Data = vodafoneModel;
                return Ok(resp);
            }

            if (ModelState.IsValid)
            {
                var model = _vfDbContext.VodafoneModels.Where(m => m.ModelId == vodafoneModel.ModelId).FirstOrDefault();
                if (model == null)
                {
                    resp.Status = "Model Id not exist.";
                    resp.Data = vodafoneModel;
                    return Ok(resp);
                }
                try
                {
                    model.ModifiedDate = DateTime.UtcNow;
                    model.Name = vodafoneModel.Name;
                    model.Description = vodafoneModel.Description;
                    _vfDbContext.VodafoneModels.Update(model);
                    _vfDbContext.SaveChanges();
                    resp.Status = "Success";
                    resp.Data = vodafoneModel;
                    return Ok(resp);
                }
                catch (Exception ex)
                {
                    resp.Status = ex.Message;
                    return BadRequest(resp);
                }
            }
            resp.Status = "Data isn't valid.";
            return BadRequest(resp);
        }
        #endregion

        #region Delete
        // POST: VodafoneModel/Delete/5
        [HttpDelete("{ModelId}")]
        public IActionResult Delete(string ModelId)
        {
            var resp = new ResponseViewModel();
            var model = _vfDbContext.VodafoneModels.Where(m => m.ModelId == ModelId).FirstOrDefault();
            if (model == null)
            {
                resp.Status = "ModelId not exist.";
                resp.Data = new { ModelId };
                return Ok(resp);
            }
            try
            {
                model.ModifiedDate = DateTime.UtcNow;
                model.DelFlag = true;
                _vfDbContext.VodafoneModels.Update(model);
                _vfDbContext.SaveChanges();
                resp.Status = "Success";
                resp.Data = model;
                return Ok(resp);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
