﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using VFSM.API.Models;
using VFSM.API.Models.AccountViewModels;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.Services;

namespace VFSM.API.Controllers.API
{
    [Produces("application/json")]
    [Route("[controller]")]
    public class AccountController : BaseController
    {

        private readonly SignInManager<ApplicationUser> _signManager;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _hostingEnv;
        private readonly IEmailService _emailSender;
        private ResponseViewModel resp;

        public AccountController(VodafoneContext vfContext,
            ApplicationDbContext context,
            IUnitOfWork unitOfWork,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signManager,
            ILogger<AccountController> logger,
            IConfiguration configuration,
            IHostingEnvironment hostingEnv,
            IEmailService emailSender)
           : base(vfContext, context, unitOfWork, userManager)
        {
            _signManager = signManager;
            _logger = logger;
            _configuration = configuration;
            _hostingEnv = hostingEnv;
            _emailSender = emailSender;
        }

        /// <summary>
        /// Set our token claims
        /// </summary>
        /// <param name="user">object user</param>
        /// <returns></returns>
        private IEnumerable<Claim> GetTokenClaims(ApplicationUser user)
        {
            return new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name, user.UserName),
            };
        }

        /// <summary>
        /// Get jwt security token
        /// </summary>
        /// <param name="user">object user</param>
        /// <returns></returns>
        private JwtSecurityToken GetJwtSecurityToken(ApplicationUser user)
        {
            var now = DateTime.UtcNow;

            // Create the credentials used to generate the token
            // Jwt:SecretKey Keysize > 128 bit with HmacSha256
            var credentials = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:SecretKey"])),
                SecurityAlgorithms.HmacSha256
                );

            return new JwtSecurityToken(
                            issuer: _configuration["Jwt:JwtIssuer"],
                            audience: _configuration["Jwt:JwtAudience"],
                            claims: GetTokenClaims(user),
                            notBefore: now,
                            expires: now.Add(TimeSpan.FromMinutes(Convert.ToInt32(_configuration["Jwt:ExpireMinutes"]))),
                            signingCredentials: credentials
                    );
        }


        /// <summary>
        /// Login
        /// </summary>
        /// <param name="model">object login</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> SignIn([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = await _vodaUserManager.FindByEmailAsync(model.Email);

                    if (user != null)
                    {
                        var istPass = await _vodaUserManager.CheckPasswordAsync(user, model.Password);
                        if (!istPass)
                            return NotFound(new { response = "PassInvalid" });

                        // Generate the Jwt token
                        var token = GetJwtSecurityToken(user);
                        return Ok(new
                        {
                            token = "Bearer " + new JwtSecurityTokenHandler().WriteToken(token),
                            username = user.UserName
                        });
                    }
                    else
                        return NotFound(new { response = "UserInvalid" });
                }
                catch(Exception ex)
                {
                    return Ok(ex.Message);
                }
                
            }
            return BadRequest(ModelState);
        }

        /// <summary>
        /// Get current user login
        /// </summary>
        /// <param name="model">object login</param>
        /// <returns></returns>
        [HttpGet]
        [Route("[action]")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> Current()
        {
            var user = await GetUserAsync();
            if (user == null)
                return Unauthorized();

            var list = _vodaUnitOfWork.Users.GetUserByUserName(user.UserName);

            return Ok(list);
        }
        #region Search
        [HttpGet]
        [Route("[action]")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Search([FromQuery] PagingViewModel page, [FromQuery] AccountSearchViewModel modelSearch)
        {
            IQueryable<VodafoneAccount> qry = null;
            qry = from user in _appDbContext.VodafoneAccount
            select new VodafoneAccount{
                Id = user.Id,
                Email = user.Email,
                EmailConfirmed = user.EmailConfirmed,
                UserName = user.UserName,
                PhoneNumber = user.PhoneNumber
            };
            if (modelSearch.Email != null)
            {
                qry = qry.Where(x => x.Email.Contains(modelSearch.Email));
            }
            switch (modelSearch.Sort)
            {
                case SortAccount.Email:
                    qry = modelSearch.SortAsc == true? qry.OrderBy(o => o.Email): qry.OrderByDescending(o => o.Email);
                    break;
                case SortAccount.UserName:
                    qry = modelSearch.SortAsc == true? qry.OrderBy(o => o.UserName): qry.OrderByDescending(o => o.UserName);
                    break;
                default:
                    qry = modelSearch.SortAsc == true? qry.OrderBy(o => o.UserName): qry.OrderByDescending(o => o.UserName);
                    break;
            }
            var pages = PageResult.GetPaged(qry, page.CurrentPage, page.PageSize, PagingSupport.DontGetTotal);
            return Ok(pages);
        }
        #endregion

        #region Detail
        [HttpGet]
        [Route("[action]/{Id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Detail(string Id)
        {
            IQueryable<AccountDetailViewModel> qry = null;
            qry = from user in _appDbContext.VodafoneAccount
                where user.Id == Id
                join ur in _appDbContext.VodafoneUserRole on user.Id equals ur.UserId into urr
                from ur in urr.DefaultIfEmpty()  
                join role in _appDbContext.VodafoneRole on ur.RoleId equals role.Id into rolee
                from role in rolee.DefaultIfEmpty()    
                select new AccountDetailViewModel
                {
                    Id = user.Id,
                    Email = user.Email,
                    EmailConfirmed = user.EmailConfirmed,
                    UserName = user.UserName,
                    PhoneNumber = user.PhoneNumber,
                    RoleId = role.Id != null? role.Id : "",
                    Role = role.Name != null? role.Name : "",
                };
            return Ok(qry.FirstOrDefault());
        }
        #endregion

        // #region Register
        // [HttpPost]
        // [AllowAnonymous]
        // [Route("[action]")]
        // // [ValidateAntiForgeryToken]
        // public async Task<IActionResult> Register([FromBody]RegisterViewModel model)
        // {
        //     if (ModelState.IsValid)
        //     {
        //         var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
        //         try
        //         {
        //             var result = await _userManager.CreateAsync(user, model.Password);
        //             if (result.Succeeded)
        //             {
        //                 _logger.LogInformation("User created a new account with password.");

        //                 var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
        //                 String callbackUrl = "";
        //                 if (_hostingEnv.EnvironmentName == "Devlopment")
        //                 {
        //                     callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
        //                 }
        //                 else
        //                 {
        //                     callbackUrl = Url.EmailConfirmationLink(user.Id, code, "https");
        //                 }
        //                 await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);

        //                 // await _signInManager.SignInAsync(user, isPersistent: false);
        //                 _logger.LogInformation("User created a new account with password.");
        //                 return Ok("Success");
        //                 // return RedirectToAction(nameof(HomeController.Contact), "Home");
        //             }
        //         }
        //         catch (System.Exception ex)
        //         {
        //             return BadRequest(ex.Message);
        //             throw;
        //         }
        //     }

        //     // If we got this far, something failed, redisplay form
        //     return BadRequest("Data not valid.");
        // }
        // #endregion

        #region Create by Admin
        [HttpPost]
        [AllowAnonymous]
        [Route("[action]")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromBody]CreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var resp = new ResponseViewModel();
                if (model.Email != model.EmailConfirm)
                {
                    resp.Status = "Error";
                    resp.Data = "Email does not match the Confirm Email.";
                    return Ok(resp);
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                try
                {
                    var oldUser = _appDbContext.VodafoneAccount.Where(o => o.Email == model.Email).FirstOrDefault();
                    if (oldUser == null)
                    {
                        // string newPassword = Guid.NewGuid().ToString();
                        var result = await _userManager.CreateAsync(user);
                        if (result.Succeeded)
                        {
                            try
                            {
                                var newAccountRole = new ApplicationUserRole
                                {
                                    UserId = user.Id,
                                    RoleId = model.RoleId
                                };
                                _appDbContext.VodafoneUserRole.Add(newAccountRole);
                                //_logger.LogInformation("User created a new account with password.");
                                //_appDbContext.SaveChanges();
                                
                                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                                SentMailResetPassword(code, model.Email, "set");
                                // await _signInManager.SignInAsync(user, isPersistent: false);
                                //_logger.LogInformation("User created a new account with password.");
                                _appDbContext.SaveChanges();
                                resp.Status = "Success";
                                var respData = new VodafoneAccount();
                                respData.Id = user.Id;
                                respData.Email = user.Email;
                                resp.Data = respData;
                                return Ok(resp);
                            }
                            catch (Exception ex)
                            {
                                resp.Status = "Error";
                                resp.Data = ex.Message;
                                return Ok(resp);
                            }
                        }
                    }
                    else
                    {   
                        resp.Status = "Error";
                        resp.Data = "Email '"+model.Email+"' is already taken.";
                        return Ok(resp);
                    }
                    
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Data not valid.");
        }
        #endregion

        #region Edit by Admin
        [HttpPut]
        [AllowAnonymous]
        [Route("[action]/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        // [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [FromBody]AccountDetailViewModel model)
        {
            if (model.Id != id)
            {
                return BadRequest("Data in valid.");
            }
            if (ModelState.IsValid)
            {
                var resp = new ResponseViewModel();
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                try
                {
                    var oldAccount = _appDbContext.VodafoneAccount.Where(o => o.Id == model.Id).FirstOrDefault();
                    if (oldAccount != null)
                    {
                        oldAccount.UserName = model.UserName;
                        oldAccount.PhoneNumber = model.PhoneNumber;
                        var oldAccountRole = _appDbContext.VodafoneUserRole.Where(o => o.UserId == oldAccount.Id).FirstOrDefault();
                        var newAccountRole = new ApplicationUserRole {
                            UserId = oldAccount.Id,
                            RoleId = model.RoleId
                        };
                        using (var scope = _appDbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                _appDbContext.VodafoneAccount.Update(oldAccount);
                                if(oldAccountRole != null)
                                {
                                    _appDbContext.VodafoneUserRole.Remove(oldAccountRole);
                                }
                                if(newAccountRole != null)
                                {
                                    _appDbContext.VodafoneUserRole.Add(newAccountRole);
                                }
                                _appDbContext.SaveChanges();

                                scope.Commit();

                                resp.Status = "Success";
                                resp.Data = model;

                                return Ok(resp);
                            }
                            catch (Exception ex)
                            {
                                scope.Rollback();
                                return BadRequest(ex.Message);
                            }
                        }
                    }
                    else
                    {   
                        resp.Status = "Error";
                        resp.Data = "Account is not exist.";
                        return Ok(resp);
                    }
                    
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return BadRequest("Data not valid.");
        }
        #endregion

        #region Remove by Admin
        [HttpDelete]
        [AllowAnonymous]
        [Route("[action]/{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        // [ValidateAntiForgeryToken]
        public IActionResult Remove(string id)
        {
            if (ModelState.IsValid)
            {
                var resp = new ResponseViewModel();
                try
                {
                    var oldAccount = _appDbContext.VodafoneAccount.Where(o => o.Id == id).FirstOrDefault();
                    if (oldAccount != null)
                    {
                        var oldAccountRole = _appDbContext.VodafoneUserRole.Where(o => o.UserId == oldAccount.Id).FirstOrDefault();
                        using (var scope = _appDbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                _appDbContext.VodafoneAccount.Remove(oldAccount);
                                if (oldAccountRole != null)
                                {
                                    _appDbContext.VodafoneUserRole.Remove(oldAccountRole);
                                }
                                _appDbContext.SaveChanges();

                                scope.Commit();

                                resp.Status = "Success";
                                resp.Data = id;

                                return Ok(resp);
                            }
                            catch (Exception ex)
                            {
                                scope.Rollback();
                                return BadRequest(ex.Message);
                            }
                        }
                    }
                    else
                    {   
                        resp.Status = "Error";
                        resp.Data = "Account is not exist.";
                        return Ok(resp);
                    }
                    
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }

            // If we got this far, something failed, redisplay form
            return BadRequest("Data not valid.");
        }
        #endregion

        // #region Comfirm Email
        // [HttpGet]
        // [Route("[action]")]
        // [AllowAnonymous]
        // public async Task<IActionResult> ConfirmEmail(string userId, string code)
        // {
        //     if (userId == null || code == null)
        //     {
        //         return BadRequest("Params null.");
        //     }
        //     var user = await _userManager.FindByIdAsync(userId);
        //     if (user == null)
        //     {
        //         throw new ApplicationException($"Unable to load user with ID '{userId}'.");
        //     }
        //     var result = await _userManager.ConfirmEmailAsync(user, code);
        //     return Ok(result.Succeeded ? "Success" : "Error");
        // }
        // #endregion

        #region Forgot password
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> ForgotPassword([FromBody]ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var resp = new ResponseViewModel();
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    resp.Status = "Account is not exited";
                    resp.Data = model;
                    return Ok(resp);
                }
                try
                {
                    // For more information on how to enable account confirmation and password reset please
                    // visit https://go.microsoft.com/fwlink/?LinkID=532713
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    SentMailResetPassword(code, model.Email, "reset");
                    resp.Status = "Success";
                    resp.Data = model;
                    return Ok(resp);
                }
                catch (Exception ex)
                {
                    resp.Status = "Error";
                    resp.Data = ex.Message;
                    return Ok(resp);
                }
            }
            // If we got this far, something failed, redisplay form
            return BadRequest("Data not valid.");
        }
        #endregion

        #region Reset Password
        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordViewModel model)
        {
            var resp = new ResponseViewModel();
            if (!ModelState.IsValid)
            {
                return Ok();
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return Ok();
            }
            try
            {
                user.EmailConfirmed = true;
                await _userManager.UpdateAsync(user);
                // String hashedNewPassword = _userManager.GetHashCode(model.Password);    
                // await store.SetPasswordHashAsync(cUser, hashedNewPassword);
                var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
                if (result.Succeeded)
                {
                    resp.Status = "Success";
                    return Ok(resp);
                }
                resp.Status = "Error";
                return Ok(resp);
            }
            catch (Exception ex)
            {
                resp.Status = ex.Message;
                return Ok(resp);
            }
        }
        #endregion
        #region Sent mail reset pass
        [HttpPost]
        [Route("[action]")]
        public async void SentMailResetPassword(string code, string email, string type)
        {
            String callbackUrl = "";
            if (_hostingEnv.EnvironmentName == "Development")
            {
                callbackUrl = "http://localhost:3000/ResetPassword?code=" + code;
            }
            else
            {
                // callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, "https");
                callbackUrl = "http://13.229.115.178:8050/ResetPassword?code=" + code;
            }
            // await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);
            if (type == "set")
            {
                await _emailSender.SendEmailAsync(email, 
                "Invite V-SOS Admin Management Site",
                $"You have been invited to the Vodafone elderly admin management site. Please set up your password by clicking here: <a href='{callbackUrl}'>link</a>");
            }
            else
            {
                await _emailSender.SendEmailAsync(email, 
                    "Reset Password",
                    $"Please reset your password by clicking here: <a href='{callbackUrl}'>link</a>");
            }
            
        }
        #endregion

    }
}