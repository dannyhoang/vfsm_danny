using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.API.Constants;
using VFSM.DAL.Models.DBModels;
using VFSM.API.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace VFSM.API.Controllers.API
{
    [Produces("application/json")]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DeviceController : BaseController
    {
        private readonly SignInManager<ApplicationUser> _signManager;
        private readonly ILogger _logger;

        public DeviceController(VodafoneContext vfContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signManager,
           ILogger<AccountController> logger)
           : base(vfContext, context, unitOfWork, userManager)
        {
            _signManager = signManager;
            _logger = logger;
        }

        #region Search
        [HttpGet]
        public IActionResult Search([FromQuery]SearchDeviceViewModel modelSearch)
        {
            var deviceList = from m in _vfDbContext.VodafoneDevices
                             select m;

            if (!String.IsNullOrEmpty(modelSearch.DeviceId))
            {
                deviceList = deviceList.Where(d => d.DeviceId.Contains(modelSearch.DeviceId));
            }
            if (!String.IsNullOrEmpty(modelSearch.Name))
            {
                deviceList = deviceList.Where(d => d.Name.Contains(modelSearch.Name));
            }
            if (!String.IsNullOrEmpty(modelSearch.DeviceUid))
            {
                deviceList = deviceList.Where(d => d.DeviceUid.Contains(modelSearch.DeviceUid));
            }

            return Ok(deviceList.ToList());
        }
        #endregion
        #region Detail
        [HttpGet]
        [Route("[action]/{deviceId}")]
        public IActionResult Detail(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                return BadRequest();
            }
            try
            {
                var query = from dev in _vfDbContext.VodafoneDevices
                            where dev.DeviceId == deviceId
                            join act in _vfDbContext.VodafoneActivities on dev.DeviceId equals act.DeviceId into actGroup
                            from actt in actGroup.DefaultIfEmpty()
                            orderby actt.CreatedDate descending
                            join b in _vfDbContext.VodafoneBeacons on dev.DeviceId equals b.DeviceId into bs
                            from beacon in bs.DefaultIfEmpty()
                            select new DeviceResponseModel
                            {
                                DeviceId = dev.DeviceId,
                                ActivityId = actt.ActivityId,
                                DeviceVersion = dev.DeviceVersion,
                                Lat = actt.Lat,
                                Lng = actt.Lng,
                                BeaconMac = beacon.Mac,
                                Battery = actt.Battery,
                                ActivatedDate = dev.ActivatedDate,
                                Status = dev.Status,
                                Imei = dev.DeviceUid,
                                DeviceName = dev.Name,
                                LatBeacon = beacon.Lat,
                                LngBeacon = beacon.Lng,
                                Version = dev.DeviceVersion
                            };

                return Ok(query.FirstOrDefault());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Create
        [HttpPost]
        public IActionResult Create([FromBody][Bind("DeviceId,DeviceVersion,ModelId,Updated,Name,DeviceUid,Imei,IsInit,ImgUrl,Battery,Status,StatusLevel,ConnServerIp,Ip,ChannelId,ActivatedDate,Activated,InactivatedDate,LandLine,Mobile,Address,City")] VodafoneDevice vodafoneDevice)
        {
            if (ModelState.IsValid)
            {
                var oldDevice = _vodaUnitOfWork.VodafoneDevices.Find(o => o.DeviceId == vodafoneDevice.DeviceId).FirstOrDefault();
                if (oldDevice == null)
                {
                    vodafoneDevice.CreatedDate = DateTime.UtcNow;
                    _vodaUnitOfWork.VodafoneDevices.Add(vodafoneDevice);
                    // await _context.SaveChangesAsync();
                    return Ok("success");
                }
                return BadRequest("Device is existing.");
            }
            return BadRequest("Data not valid.");
        }
        #endregion

        #region Edit

        [HttpPut]
        public IActionResult Edit([FromBody] DeviceViewModel vodafoneDevice)
        {
            var resp = new ResponseViewModel();
            if (string.IsNullOrEmpty(vodafoneDevice.DeviceId))
            {
                resp.Status = "DeviceId can't null.";
                resp.Data = vodafoneDevice;
                return Ok(resp);
            }

            if (ModelState.IsValid)
            {
                var device = _vfDbContext.VodafoneDevices.Where(u => u.DeviceId == vodafoneDevice.DeviceId).FirstOrDefault();
                try
                {
                    device.Name = vodafoneDevice.Name;
                    device.ModifiedDate = DateTime.UtcNow;
                    _vfDbContext.VodafoneDevices.Update(device);
                    _vfDbContext.SaveChanges();
                    return Ok(vodafoneDevice);
                }
                catch (Exception Ex)
                {
                    BadRequest(Ex.Message);
                };
            }
            return BadRequest("Data is not valid.");
        }
        #endregion
    }
}