﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using VFSM.DAL;
using VFSM.DAL.Models;

namespace VFSM.API.Controllers.API
{
    [Produces("application/json")]
    [Route("[controller]")]
    public class HomeController : BaseController
    {

        private readonly SignInManager<ApplicationUser> _signManager;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        public HomeController(VodafoneContext vfContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signManager,
           ILogger<AccountController> logger,
           IConfiguration configuration)
           : base(vfContext, context, unitOfWork, userManager)
        {
            _signManager = signManager;
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        [Route("~/")]
        public async Task<IActionResult> Index()
        {
            return Content("VODA FONE API Home");
        }


    }
}