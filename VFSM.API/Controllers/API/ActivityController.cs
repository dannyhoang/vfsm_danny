using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.API.Constants;
using VFSM.DAL.Models.DBModels;
using VFSM.API.Models.UserViewModels;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;
using VFSM.API.Models;
using VFSM.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace VFSM.API.Controllers.API
{
    //[Authorize(Roles = "Admin, CS Read, CS Write")]
    [Produces("application/json")]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ActivityController : BaseController
    {

        public ActivityController(VodafoneContext vfContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager)
           : base(vfContext, context, unitOfWork, userManager)
        {
        }

        #region Search
        [HttpGet]
        public IActionResult Search([FromQuery] PagingViewModel page, [FromQuery] ActivitySearchViewModels modelSearch)
        {
            if (modelSearch.FromDate == default(DateTime))
            {
                modelSearch.FromDate = DateTime.Today.AddMonths(-6);
            }
            if (modelSearch.ToDate == default(DateTime))
            {
                modelSearch.ToDate = DateTime.Today;
            }
            if (ModelState.IsValid)
            {
                var result = new ActivityResult();
                IQueryable<DeviceActivityViewModel> qryDevice = null;
                qryDevice = _vfDbContext.VodafoneDevices
                            .Where(d => d.Activated == true
                            && d.ActivatedDate >= new DateTime(modelSearch.FromDate.Year, modelSearch.FromDate.Month, modelSearch.FromDate.Day, 0, 0, 0)
                            && d.ActivatedDate <= new DateTime(modelSearch.ToDate.Year, modelSearch.ToDate.Month, modelSearch.ToDate.Day, 23, 59, 59))
                            .Select(d => new DeviceActivityViewModel
                            {
                                DeviceId = d.DeviceId,
                                Imei = d.DeviceUid,
                                Battery = d.Battery,
                                Activated = d.Activated,
                                ActivatedDate = Convert.ToDateTime(d.ActivatedDate)
                            });
                // var listDeviceId = new List<string>();
                // Search by Activity
                if (modelSearch.ActivityId != null)
                {
                    var listDeviceByAct = (_vfDbContext.VodafoneActivities.Where(x => x.ActivityId.Contains(modelSearch.ActivityId)).GroupBy(a => a.DeviceId)
                    .Select(a => a.First().DeviceId)).ToList();
                    qryDevice = qryDevice.Where(d => listDeviceByAct.Contains(d.DeviceId));
                }
                else if (modelSearch.EventType != null)
                {
                    var listDeviceByAct = (_vfDbContext.VodafoneActivities.Where(x => x.EventType.Contains(modelSearch.EventType)).GroupBy(a => a.DeviceId)
                    .Select(a => a.First().DeviceId)).ToList();
                    qryDevice = qryDevice.Where(d => listDeviceByAct.Contains(d.DeviceId));
                }
                else
                {
                    // Search by Device
                    if (modelSearch.DeviceId != null)
                    {
                        qryDevice = qryDevice.Where(x => x.DeviceId.Contains(modelSearch.DeviceId));
                    }
                    if (modelSearch.UserId != null)
                    {
                        qryDevice = qryDevice.Where(x => (from ud in _vfDbContext.VodafoneUserDevices
                                                          where ud.UserId.Contains(modelSearch.UserId)
                                                          select ud.DeviceId).Contains(x.DeviceId));
                    }
                    if (modelSearch.Imei != null)
                    {
                        qryDevice = qryDevice.Where(x => x.Imei.Contains(modelSearch.Imei));
                    }
                    if (modelSearch.Mac != null)
                    {
                        qryDevice = qryDevice.Where(x => (from be in _vfDbContext.VodafoneBeacons
                                                          where be.Mac.Contains(modelSearch.Mac)
                                                          select be.DeviceId).Contains(x.DeviceId));
                    }
                };
                // Sort
                if (modelSearch.Sort != null && modelSearch.Sort != "")
                {
                    switch (modelSearch.Sort.ToUpper())
                    {
                        case SortActivity.ActivatedDate:
                            qryDevice = modelSearch.SortAsc == true ? qryDevice.OrderBy(o => o.ActivatedDate) : qryDevice.OrderByDescending(o => o.ActivatedDate);
                            break;
                        case SortActivity.Imei:
                            qryDevice = modelSearch.SortAsc == true ? qryDevice.OrderBy(o => o.Imei) : qryDevice.OrderByDescending(o => o.Imei);
                            break;
                        default:
                            qryDevice = qryDevice.OrderByDescending(o => o.ActivatedDate);
                            break;
                    }
                }
                // qryDevice = qryDevice.GroupBy(x => x.DeviceId).Select(x => x.FirstOrDefault());
                // Paging
                var pages = PageResult.GetPaged(qryDevice, page.CurrentPage, page.PageSize, PagingSupport.GetTotal);
                // Fill data
                List<ActivityViewModels> listAct = GetListActCheckSosAndFall(pages.Items.ToList());
                result = new ActivityResult
                {
                    Items = listAct,
                    CurrentPage = pages.CurrentPage,
                    PageSize = pages.PageSize,
                    PageCount = pages.PageCount,
                    TotalItems = pages.TotalItems
                };
                return Ok(result);
            }
            return BadRequest("Data not valid.");
        }
        #endregion

        #region Support search
        [HttpGet("{userId}")]
        public List<ActivityViewModels> GetListActCheckSosAndFall(List<DeviceActivityViewModel> Items)
        {
            var listDeviceId = new List<string>();
            foreach (var item in Items)
            {
                listDeviceId.Add(item.DeviceId);
            }
            var activity = (from act in _vfDbContext.VodafoneActivities
                            where listDeviceId.Contains(act.DeviceId)
                            join d in _vfDbContext.VodafoneDevices on act.DeviceId equals d.DeviceId
                            orderby act.CreatedDate descending
                            select new ActivityViewModels
                            {
                                DeviceId = d.DeviceId,
                                Imei = d.DeviceUid,
                                LastActivityId = act.ActivityId,
                                LastEventType = act.EventType,
                                Battery = act.Battery,
                                ActivatedDate = d.ActivatedDate
                            }).ToList();

            var listAct = new List<ActivityViewModels>();
            foreach (var item in listDeviceId)
            {
                var act = activity.Where(o => o.DeviceId == item).ToList();
                if (act.Count() > 0)
                {
                    var actObject = act[0];
                    actObject.HaveSos = (act.Where(o => o.LastEventType.Contains("SOS"))).Count() > 0 ? true : false;
                    actObject.HaveFall = (act.Where(o => o.LastEventType.Contains("FALL"))).Count() > 0 ? true : false;
                    listAct.Add(actObject);
                }
                else
                {
                    var actO = new ActivityViewModels
                    {
                        DeviceId = Items.Find(o => o.DeviceId == item).DeviceId,
                        Imei = Items.Find(o => o.DeviceId == item).Imei,
                        ActivatedDate = Items.Find(o => o.DeviceId == item).ActivatedDate
                    };
                    listAct.Add(actO);
                }
            }
            return listAct;
        }
        #endregion
        #region Detail
        [HttpGet]
        [Route("[action]/{deviceId}")]
        public IActionResult GetByDeviceId(string deviceId, [FromQuery] string Type)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                return BadRequest();
            }
            try
            {
                List<string> listEventType = new List<string>();
                if (Type != null)
                {
                    switch (Type.ToUpper().ToString())
                    {
                        case TypeActivity.History:
                            listEventType = new List<string>(new string[] { "SWITCH_ON", "SWITCH_OFF", "REMOVAL", "PUT_ON", "EMERGENCY_CANCEL" });
                            break;
                        case TypeActivity.SOS:
                            listEventType.Add(TypeActivity.SOS);
                            break;
                        case TypeActivity.FALL:
                            listEventType.Add(TypeActivity.FALL);
                            break;
                        case TypeActivity.Battery:
                            listEventType = new List<string>(new string[] { "CHARGE_BATTERY", "LOW_BATTERY" });
                            break;
                        default:
                            listEventType = new List<string>(new string[] { "SWITCH_ON", "SWITCH_OFF", "REMOVAL", "PUT_ON" });
                            break;
                    }
                }
                var query = from act in _vfDbContext.VodafoneActivities
                            where act.DeviceId == deviceId && listEventType.Contains(act.EventType)
                            join beacon in _vfDbContext.VodafoneBeacons on act.DeviceId equals beacon.DeviceId into beaconn
                            from beaconN in beaconn.DefaultIfEmpty()
                            orderby act.CreatedDate descending
                            select new ActivityByDeviceViewModels
                            {
                                DeviceId = act.DeviceId,
                                ActivityId = act.ActivityId,
                                EventType = act.EventType,
                                Lat = act.Lat,
                                Lng = act.Lng,
                                Battery = act.Battery,
                                CreatedDate = Convert.ToDateTime(act.CreatedDate),
                                LatBeacon = beaconN.Lat,
                                LngBeacon = beaconN.Lng,
                                Accuracy = act.Accuracy,
                                LocSource = act.LocSource
                            };

                return Ok(query.ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
        #region Get last act
        [HttpGet]
        [Route("[action]/{deviceId}")]
        public IActionResult LastAct(string deviceId)
        {
            if (string.IsNullOrEmpty(deviceId))
            {
                return BadRequest();
            }
            try
            {
                var query = from act in _vfDbContext.VodafoneActivities
                            where act.DeviceId == deviceId
                            join beacon in _vfDbContext.VodafoneBeacons on act.DeviceId equals beacon.DeviceId into beaconn
                            from beaconN in beaconn.DefaultIfEmpty()
                            orderby act.CreatedDate descending
                            select new ActivityByDeviceViewModels
                            {
                                DeviceId = act.DeviceId,
                                ActivityId = act.ActivityId,
                                EventType = act.EventType,
                                Lat = act.Lat,
                                Lng = act.Lng,
                                Battery = act.Battery,
                                CreatedDate = Convert.ToDateTime(act.CreatedDate),
                                LatBeacon = beaconN.Lat,
                                LngBeacon = beaconN.Lng,
                                LocSource = act.LocSource
                            };

                return Ok(query.FirstOrDefault());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}