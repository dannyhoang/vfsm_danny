using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL;
using VFSM.DAL.Models;
using VFSM.API.Constants;
using VFSM.DAL.Models.DBModels;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Routing;
using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using VFSM.API.Config;
using Microsoft.Extensions.Options;
using VFSM.Services.Extensions;
using VFSM.API.Models;
using System.IO;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Amazon;
using Amazon.Runtime;

namespace VFSM.API.Controllers.API
{
    //[Authorize(Roles = "Admin, Service Managements")]
    [Produces("application/json")]
    [Route("[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OTAController : BaseController
    {
        private readonly AWSConfig _awsConfig;
        private TransferUtility FileTransferUtility;
        private IAmazonS3 _s3Client;
        private IConfiguration _configuration;

        public OTAController(VodafoneContext vfContext,
            ApplicationDbContext context,
            IUnitOfWork unitOfWork,
            UserManager<ApplicationUser> userManager,
            IAmazonS3 s3Client, 
            IOptions<AWSConfig> awsConfig,
            IConfiguration configuration)
           : base(vfContext, context, unitOfWork, userManager)
        {            
            _awsConfig = awsConfig.Value;
            _configuration = configuration;
            // FileTransferUtility = new TransferUtility("AKIAJ26YP7LCCLGTUZ2Q", "Wm3dt/TaY2MVZNOzS4ERQbqmgH3CQt1fw6gAO1m3", RegionEndpoint.APSoutheast1);
            var credentials = new BasicAWSCredentials("AKIAJ26YP7LCCLGTUZ2Q", "Wm3dt/TaY2MVZNOzS4ERQbqmgH3CQt1fw6gAO1m3");
            _s3Client = new AmazonS3Client(credentials, RegionEndpoint.APSoutheast1);
        }

        #region Search
        [HttpGet]
        [Route("[action]")]
        public IActionResult Search([FromQuery]OTAViewModel modelSearch)
        {
            var qry = _vfDbContext.VodafoneOtas.AsNoTracking();

            if (!String.IsNullOrEmpty(modelSearch.ModelId))
            {
                qry = qry.Where(o => o.ModelId == modelSearch.ModelId);
            }
            if (!String.IsNullOrEmpty(modelSearch.Category))
            {
                qry = qry.Where(o => o.Category == modelSearch.Category);
            }
            if (!String.IsNullOrEmpty(modelSearch.Version))
            {
                qry = qry.Where(o => o.Version == modelSearch.Version);
            }

            return Ok(qry.ToList());
        }
        #endregion

        #region Get by model id
        [HttpGet]
        [Route("[action]/{modelId}")]
        public IActionResult getByModelId(string modelId)
        {
            var qry = (_vfDbContext.VodafoneOtas.Where(o => o.ModelId == modelId)).ToList();
            foreach (var item in qry)
            {
                if(item.ModifiedDate == null)
                {
                    item.ModifiedDate = item.CreatedDate;
                }
            }
            
            return Ok(qry.OrderByDescending(si => si.ModifiedDate));
        }
        #endregion

        #region Detail
        [HttpGet("{modelId}/{category}/{version}")]
        //[Route("[action]")]
        public IActionResult Details(string modelId, string category, string version)
        {
            var ota = _vodaUnitOfWork.VodafoneModelOTA.SingleOrDefault(o => o.ModelId == modelId && o.Category == category && o.Version == version);
            if (ota == null)
            {
                return BadRequest("Data not exist.");
            }
            return Ok(ota);
        }
        #endregion

        #region Create
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]OTACreateViewModel ota)
        {
            var resp = new ResponseViewModel();
            // TODO: file model validation check
            if (ota.ModelId == "" || ota.ModelId == null || !VodafoneModelExists(ota.ModelId))
            {
                resp.Status = "Error";
                resp.Data = "Model not found. Check your Model ID.";
                return Ok(resp);
            }
            if (VodafoneOTAExists(ota.ModelId, ota.Category, ota.Version))
            {
                resp.Status = "Error";
                resp.Data = "Duplicate version.";
                return Ok(resp);
            }
            if (ota.Category == "MC60" && VodafoneOTAMappingVersionExists(ota.MappingVersion))
            {
                resp.Status = "Error";
                resp.Data = "Duplicate mapping version.";
                return Ok(resp);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var bytes = Convert.FromBase64String(ota.File.FileBase64);
                    var contents = new MemoryStream(bytes);
                    // TODO: AWS file upload
                    var FileTransferUtility = new TransferUtility(_s3Client);
                    // await FileTransferUtility.UploadAsync(
                    //             contents,
                    //             "vfeb",
                    //             AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name));
                    var requestS3 = new TransferUtilityUploadRequest
                    {
                        BucketName = _configuration["AWSConfig:AWS_BUCKET"],
                        Key = AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name),
                        InputStream = contents
                    };

                    // AWS file upload
                    FileTransferUtility.Upload(requestS3);

                    GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
                    request.BucketName = _configuration["AWSConfig:AWS_BUCKET"];
                    request.Key = AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name);
                    request.Expires = DateTime.Now.AddHours(1);
                    request.Protocol = Protocol.HTTPS;

                    var url = new Uri(FileTransferUtility.S3Client.GetPreSignedURL(request));

                    // TODO: Save uploaded info to T_MODEL_OTA table
                    var otaModel = new VodafoneModelOTA
                    {
                        ModelId = ota.ModelId,
                        Version = ota.Version,
                        Category = ota.Category,
                        IsActive = ota.IsActive? ota.IsActive: false,
                        OtaUrl = url.Scheme + "://" + url.Host + url.LocalPath,
                        FileName = AWSS3PathExtensions.GenerateFilenameWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name),
                        FileSize = ota.File.Size,
                        CreatedDate = DateTime.Now,
                        MappingVersion = ota.MappingVersion
                    };
                    _vfDbContext.VodafoneOtas.Add(otaModel);
                    _vfDbContext.SaveChanges();
                    resp.Status = "Success";
                    resp.Data = new
                    {
                        ota.ModelId,
                        ota.Version,
                        ota.Category,
                        ota.IsActive,
                        fileSize = ota.File.Size,
                        modifiedDate = DateTime.Now,
                        ota.MappingVersion,
                        otaModel.OtaUrl
                    };
                    return Ok(resp);
                }
                catch (Exception ex)
                {
                    resp.Status = ex.Message;
                    return BadRequest(resp);
                }
            }
            resp.Status = "Data not valid.";
            return BadRequest(resp);
        }
        #endregion
        
        #region Edit
        [HttpPut]
        public async Task<IActionResult> Edit([FromBody]OTAUpdateViewModel ota)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // TODO: AWS file upload
                    var FileTransferUtility = new TransferUtility(_s3Client);
                    var resp = new ResponseViewModel();
                    var originalOta = await _vfDbContext.VodafoneOtas.SingleOrDefaultAsync(o => o.ModelId == ota.ModelId && o.Version == ota.Version && o.Category == ota.Category);
                    if (ota.File.Size != 0)
                    {
                        var bytes = Convert.FromBase64String(ota.File.FileBase64);
                        var contents = new MemoryStream(bytes);
                        if (!string.IsNullOrEmpty(originalOta.OtaUrl))
                        {
                            var uri = new Uri(originalOta.OtaUrl);
                            string[] splitUri = uri.LocalPath.Split("/");
                            DeleteObjectRequest deleteObjectRequest = null;
                            if (splitUri.Length == 4){
                                
                                deleteObjectRequest =  new DeleteObjectRequest
                                        {
                                            BucketName = _configuration["AWSConfig:AWS_BUCKET"],
                                            Key = AWSS3PathExtensions.GenerateDeleteAWSS3KeyWithoutCategory(originalOta.ModelId, originalOta.Version, originalOta.FileName)
                                        };
                            } else {
                                deleteObjectRequest =
                                    new DeleteObjectRequest
                                        {
                                            BucketName = _configuration["AWSConfig:AWS_BUCKET"],
                                            Key = AWSS3PathExtensions.GenerateDeleteAWSS3KeyWithCategory(originalOta.ModelId, originalOta.Version, originalOta.Category, originalOta.FileName)
                                        };
                            }
                            await FileTransferUtility.S3Client.DeleteObjectAsync(deleteObjectRequest);
                        }
                        // await FileTransferUtility.UploadAsync(
                        //             contents,
                        //             "vfeb",
                        //             AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name));
                        var requestS3 = new TransferUtilityUploadRequest
                        {
                            BucketName = _configuration["AWSConfig:AWS_BUCKET"],
                            Key = AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name),
                            InputStream = contents
                        };

                        // AWS file upload
                        FileTransferUtility.Upload(requestS3);
                        // await FileTransferUtility.UploadAsync(
                        //     contents,
                        //     _configuration["AWSConfig:AWS_BUCKET"], 
                        //     AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name));
                
                        GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
                        request.BucketName = _configuration["AWSConfig:AWS_BUCKET"];
                        request.Key = AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name);
                        request.Expires = DateTime.Now.AddHours(1);
                        request.Protocol = Protocol.HTTPS;
                        
                        var url = new Uri(FileTransferUtility.S3Client.GetPreSignedURL(request));
                        
                        originalOta.IsActive = ota.IsActive;
                        originalOta.OtaUrl = url.Scheme + "://" + url.Host + url.LocalPath;
                        originalOta.FileName = AWSS3PathExtensions.GenerateFilenameWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.Name);
                        originalOta.FileSize = ota.File.Size;
                        originalOta.MappingVersion = ota.MappingVersion;
                    }
                    else 
                    {
                        originalOta.IsActive = ota.IsActive;
                        originalOta.MappingVersion = ota.MappingVersion;
                    }

                    originalOta.ModifiedDate = DateTime.Now;
                    _vfDbContext.VodafoneOtas.Update(originalOta);
                    _vfDbContext.SaveChanges();
                    resp.Status = "Success";
                    resp.Data = originalOta;
                    return Ok(resp);
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            return BadRequest("Data not valid.");
        }
        #endregion

        #region Delete
        // POST: VodafoneModel/Delete/5
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] List<OTADeleteViewModel> delList)
        {
            if (ModelState.IsValid)
            {
                var resp = new ResponseViewModel();
                try
                {
                    var listSuccess = new List<OTADeleteViewModel>();
                    var listFail = new List<OTADeleteViewModel>();
                    foreach (var modelDelete in delList)
                    {
                        var ota = await _vfDbContext.VodafoneOtas.SingleOrDefaultAsync(o => o.ModelId == modelDelete.ModelId && o.Category == modelDelete.Category && o.Version == modelDelete.Version);
                        if (ota != null)
                        {

                            _vfDbContext.VodafoneOtas.Remove(ota);
                            _vfDbContext.SaveChanges();
                            listSuccess.Add(modelDelete);
                            try
                            {
                                if (!string.IsNullOrEmpty(ota.OtaUrl))
                                {
                                    var uri = new Uri(ota.OtaUrl);
                                    string[] splitUri = uri.LocalPath.Split("/");
                                    DeleteObjectRequest deleteObjectRequest = null;
                                    if (splitUri.Length == 4)
                                    {

                                        deleteObjectRequest = new DeleteObjectRequest
                                        {
                                            BucketName = _configuration["AWSConfig:AWS_BUCKET"],
                                            Key = AWSS3PathExtensions.GenerateDeleteAWSS3KeyWithoutCategory(ota.ModelId, ota.Version, ota.FileName)
                                        };
                                    }
                                    else
                                    {
                                        deleteObjectRequest = new DeleteObjectRequest
                                        {
                                            BucketName = _configuration["AWSConfig:AWS_BUCKET"],
                                            Key = AWSS3PathExtensions.GenerateDeleteAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.FileName)
                                        };
                                    }

                                    await FileTransferUtility.S3Client.DeleteObjectAsync(deleteObjectRequest);
                                }
                            }
                            catch
                            {
                                continue;
                            }
                        }
                        else
                        {
                            listFail.Add(modelDelete);
                        }
                    }
                    resp.Data = new { listSuccess, listFail };
                    return Ok(resp);
                }
                catch (Exception ex)
                {
                    resp.Status = ex.Message;
                    return Ok(resp);
                }
            }
            return BadRequest("Data is not valid.");
        }
        #endregion


    }
}