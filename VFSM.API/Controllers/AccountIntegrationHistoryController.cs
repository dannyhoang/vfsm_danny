using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;
using VFSM.API.Data;
using VFSM.API.Constants;
using VFSM.DAL.Models;
using VFSM.DAL.Models.DBModels;


namespace VFSM.API.Controllers
{
    [Authorize(Roles = "Admin, CS Read, CS Write")]
    public class AccountIntegrationHistoryController : BaseController
    {
        public AccountIntegrationHistoryController(VodafoneContext context, UserManager<ApplicationUser> userManager)
        :base(context, userManager)
        {}

        #region Index
        [HttpGet]
        public async Task<IActionResult> Index(string filter, string sortExpression = "-ReqDate", int page = 1)
        {
            var qry = _context.VodafoneAccountIntegrationHistories.AsNoTracking();
            if (!string.IsNullOrEmpty(filter))
            {
                qry = qry.Where(m => m.Imei.Contains(filter));
            }
            var model = await PagingList<VodafoneAccountIntegrationHistory>.CreateAsync(qry, VF_CONSTANT.PageSize, page, sortExpression, "-ReqDate");
            model.RouteValue = new RouteValueDictionary { {"filter", filter} };
            return View(model);
        }
        #endregion
    }
}