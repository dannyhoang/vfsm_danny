using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.DAL.Models.DBModels
{
    [Table("T_ACCT_INTE_HIS")]
    public class VodafoneAccountIntegrationHistory
    {
        [Key]
        [Required]
        [Column("SEQ")]
        [Display(Name = "Seq")]
        public int Seq { get; set; }

        [Column("IMEI")]
        [StringLength(64)]
        [Display(Name = "Imei")]
        public string Imei { get; set; }

        [Column("USER_ID")]
        [StringLength(64)]
        [Display(Name = "Customer Id")]
        public string UserId { get; set; }

        [Column("STATE")]
        [StringLength(24)]
        [Display(Name = "State")]
        public string State { get; set; }

        [Column("SUCCEED", TypeName = "bit")]
        [Display(Name = "Result")]
        public bool Succeed { get; set; }

        [Column("REQ_BODY")]
        [StringLength(1024)]
        [Display(Name = "Requst Body")]
        public string ReqBody { get; set; }

        [Column("RESPONSE")]
        [StringLength(1024)]
        [Display(Name = "Response")]
        public string Response { get; set; }

        [Column("IP")]
        [StringLength(24)]
        [Display(Name = "IP")]
        public string IP { get; set; }

        [Column("REQ_DATE")]
        [Display(Name = "Request Date")]
        public DateTime? ReqDate { get; set; }

        [Column("EXE_TIME")]
        [Display(Name = "Execution Time")]
        public int ExeTime { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? CreatedDate { get; set; }

        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }
    }
}