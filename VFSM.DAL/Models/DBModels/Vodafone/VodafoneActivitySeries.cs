using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.DAL.Models.DBModels
{
    [Table("T_ACTIVITY_SERIES")]
    public class VodafoneActivitySeries
    {
        [Key]
        [Required]
        [Column("ACT_SERIES_SEQ")]
        [Display(Name = "Sequence")]
        public int ActSeriesSeq { get; set; }

        [Required]
        [Column("ACT_ID")]
        [StringLength(36)]
        [Display(Name = "Activity Id")]
        public string ActId { get; set; }

        [Column("USER_ID")]
        [StringLength(36)]
        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Column("STATUS_LEVEL")]
        [Display(Name = "Status Level")]
        public string StatusLevel { get; set; }

        [Column("MESSAGE")]
        [Display(Name = "Message")]
        public string Message { get; set; }

        [Required]
        [Column("LOC_SOURCE")]
        [Display(Name = "Log Source")]
        public string LogSource { get; set; }

        [Column("LAT")]
        [Display(Name = "LAT")]
        public double Lat { get; set; }

        [Column("LNG")]
        [Display(Name = "LNG")]
        public double Lng { get; set; }

        [Required]
        [Column("ACTION")]
        [Display(Name = "Action")]
        public string Action { get; set; }

        [Column("BATTERY")]
        [Display(Name = "Battery")]
        public int Battery { get; set; }

        [Column("ACCURACY")]
        [Display(Name = "Accuracy")]
        public double? Accuracy { get; set; }

        [Required]
        [Column("RESOLVED", TypeName = "bit")]
        [DefaultValue(false)]
        [Display(Name = "Resolved")]
        public bool  Resolved { get; set; }

        [Column("UNI_ACT")]
        [StringLength(18)]
        [Display(Name = "UniAct")]
        public string UniAct { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? CreatedDate { get; set; }

        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }

        [Column("DEVICE_ID")]
        [StringLength(36)]
        [Display(Name = "Device Id")]
        public string DeviceId { get; set; }
    }
}