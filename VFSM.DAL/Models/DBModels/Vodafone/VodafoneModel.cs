using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.DAL.Models.DBModels
{
    [Table("T_MODEL")]
    public class VodafoneModel
    {
        [Key]
        [Column("MODEL_ID")]
        [StringLength(20)]
        [Display(Name = "Model ID")]
        public string ModelId { get; set; }

        [Required]
        [Column("NAME")]
        [StringLength(255)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? CreateDate { get; set; }

        [Column("DESCRIPTION")]
        [Display(Name = "Description")]

        public string Description { get; set; }

        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }

        [Required]
        [Column("DEL_FLAG")]
        [Display(Name = "Deleted")]
        public bool DelFlag { get; set; }
    }
}