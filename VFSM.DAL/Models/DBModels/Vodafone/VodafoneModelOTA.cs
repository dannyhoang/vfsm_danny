using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.DAL.Models.DBModels
{
    [Table("T_MODEL_OTA")]
    public class VodafoneModelOTA
    {
        [Key, Column("MODEL_ID", Order = 0)]
        [Required]
        [StringLength(20)]
        [Display(Name = "Model Id")]
        public string ModelId { get; set; }

        [Key, Column("VERSION", Order = 1)]
        [Required]
        [StringLength(20)]
        [Display(Name = "SW Version")]
        public string Version { get; set; }

        [Key, Column("CATEGORY", Order = 2)]
        [Required]
        [StringLength(20)]
        [Display(Name = "Category")]
        public string Category { get;set; }
        
        [Required]
        [Column("IS_ACTIVE", TypeName="bit")]
        [DefaultValue(false)]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Column("OTA_URL")]
        [Display(Name = "URL")]
        public string OtaUrl { get; set; }

        [Required]
        [Column("FILE_NAME")]
        [StringLength(200)]
        [Display(Name = "File name")]
        public string FileName { get; set; }

        [Required]
        [Column("FILE_SIZE", TypeName="double")]
        [Display(Name = "File size")]
        public double FileSize { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Create Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }

        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }

        [Column("MAPPING_VERSION")]
        [Display(Name = "Mapping version")]
        public string MappingVersion { get; set; }
    }
}