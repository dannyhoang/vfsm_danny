using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.DAL.Models.DBModels
{
    [Table("T_USER")]
    public class VodafoneUser
    {
        [Key]
        [Required]
        [Column("USER_ID")]
        [StringLength(36)]
        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Required]
        [Column("NICK_NAME")]
        [StringLength(36)]
        [Display(Name = "Nick Name")]
        public string NickName { get; set; }

        [Column("PHONE_NUMBER")]
        [StringLength(24)]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        [Column("IMG_URL")]
        [StringLength(255)]
        [Display(Name = "Image Url")]
        public string ImgUrl { get; set; }

        [Required]
        [Column("LANG_CODE")]
        [StringLength(64)]
        [Display(Name = "Lang Code")]
        public string LangCode { get; set; }

        [Column("IMEI")]
        [StringLength(128)]
        [Display(Name = "IMEI")]
        public string Imei { get; set; }

        [Column("APP_PUSH_ID")]
        [StringLength(516)]
        [Display(Name = "App Push Id")]
        public string AppPushId { get; set; }

        [Required]
        [Column("MOBILE_TYPE")]
        [StringLength(12)]
        [Display(Name = "Mobile Type")]
        public string MobileType { get; set; }

        [Column("MOBILE_MODEL")]
        [StringLength(128)]
        [Display(Name = "Mobile Model")]
        public string MobileModel { get; set; }

        [Column("PUSH_TYPE")]
        [StringLength(12)]
        [Display(Name = "Push Type")]
        public string PushType { get; set; }

        [Column("END_POINT_ARN")]
        [StringLength(512)]
        [Display(Name = "End Point ARN")]
        public string EndPointArn { get; set; }

        [Column("END_POINT_ARN_UPDATED_DATE")]
        [Display(Name = "End Point ARN Updated Date")]
        public DateTime? EndPointArnUpdatedDate { get; set; }

        [Required]
        [Column("EXPIRED", TypeName = "bit")]
        [DefaultValue(false)]
        [Display(Name = "Expired")]
        public bool Expired { get; set; }

        [Column("EXPIRY_DATE")]
        [Display(Name = "Expiry Date")]
        public DateTime? ExpiryDate { get; set; }

        [Required]
        [Column("LOGON", TypeName = "bit")]
        [DefaultValue(true)]
        [Display(Name = "Logon")]
        public bool Logon { get; set; }

        [Column("LAST_LOGIN_DATE")]
        [Display(Name = "Last Login Date")]
        public DateTime? LastLoginDate { get; set; }

        [Column("SESSION_CODE")]
        [StringLength(128)]
        [Display(Name = "Session Code")]
        public string SessionCode { get; set; }

        [Column("USER_AGENT")]
        [StringLength(128)]
        [Display(Name = "User Agent")]
        public string UserAgent { get; set; }

        [Required]
        [Column("VERSION_CODE")]
        [StringLength(64)]
        [Display(Name = "Version Code")]
        public string VersionCode { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Create Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }
        
        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }
    }
}