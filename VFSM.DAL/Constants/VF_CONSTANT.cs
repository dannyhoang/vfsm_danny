
namespace VFSM.DAL.Constants
{
    public static class VF_CONSTANT
    {
        // Default Page Size : 10
        public const int PageSize = 10;

        // Release
        public const string Release = "Release";

        // Development
        public const string Development = "Development";

        // Staging
        public const string Staging = "Staging";

        // MC60
        public const string MC60 = "MC60";

        // FIRMWARE
        public const string FIRMWARE = "FIRMWARE";

        // All Categories
        public const string AllCategories = "All Categories";

        // All Models
        public const string AllModels = "All Models";
    }
}