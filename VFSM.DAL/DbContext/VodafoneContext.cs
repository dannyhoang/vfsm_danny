using Microsoft.EntityFrameworkCore;
using VFSM.DAL.Models.DBModels;

namespace VFSM.DAL
{
    public class VodafoneContext : DbContext
    {
        public VodafoneContext (DbContextOptions<VodafoneContext> options)
            : base(options)
        {
        }

        public DbSet<VodafoneModel> VodafoneModels { get; set; }

        public DbSet<VodafoneDevice> VodafoneDevices { get; set; }

        public DbSet<VodafoneModelOTA> VodafoneOtas { get; set; }

        public DbSet<VodafoneUser> VodafoneUsers { get; set; }

        public DbSet<VodafoneUserDevice> VodafoneUserDevices { get; set; }

        public DbSet<VodafoneActivity> VodafoneActivities { get; set; }

        public DbSet<VodafoneActivitySeries> VodafoneActivitySeries { get; set; }

        public DbSet<VodafoneBeacon> VodafoneBeacons { get; set; }

        public DbSet<VodafoneAccountIntegrationHistory> VodafoneAccountIntegrationHistories { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // T_MODEL_OTA 테이블 복합키 설정
            modelBuilder.Entity<VodafoneModelOTA>()
                .HasKey(o => new {o.ModelId, o.Version, o.Category});
            
            // T_USER_DEVICE 테이블 복합키 설정
            modelBuilder.Entity<VodafoneUserDevice>()
                .HasKey(o => new {o.UserId, o.DeviceId});
            base.OnModelCreating(modelBuilder);
        }
    }
}