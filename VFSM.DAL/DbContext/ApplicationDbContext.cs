﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using VFSM.DAL.Models;
using VFSM.DAL.Models.DBModels;

namespace VFSM.DAL
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserClaim<string>,
    ApplicationUserRole, IdentityUserLogin<string>,
    IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public string CurrentUserName { get; set; }
        public string Email { get; set; }

        public DbSet<VodafonePage> VodafonePages { get; set; }
        public DbSet<VodafonePageOperation> VodafonePageOperations { get; set; }
        public DbSet<VodafonePageRole> VodafonePageRoles { get; set; }
        public DbSet<VodafoneOperation> VodafoneOperations { get; set; }
        public DbSet<ApplicationUser> VodafoneAccount { get; set; }
        public DbSet<ApplicationRole> VodafoneRole { get; set; }
        public DbSet<ApplicationUserRole> VodafoneUserRole { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().ToTable("T_ADMIN_USERS");
            builder.Entity<ApplicationRole>().ToTable("T_ADMIN_ROLES");
            builder.Entity<ApplicationUserRole>().ToTable("T_ADMIN_USER_ROLES");
            builder.Entity<IdentityUserClaim<string>>().ToTable("T_ADMIN_USER_CLAIMS");
            builder.Entity<IdentityUserLogin<string>>().ToTable("T_ADMIN_USER_LOGINS");
            builder.Entity<IdentityRoleClaim<string>>().ToTable("T_ADMIN_ROLE_CLAIMS");
            builder.Entity<IdentityUserToken<string>>().ToTable("T_ADMIN_USER_TOKENS");

            builder.Entity<VodafonePage>().ToTable("T_ADMIN_PAGE");
            builder.Entity<VodafonePageOperation>().ToTable("T_ADMIN_PAGE_OPERATION");
            builder.Entity<VodafonePageRole>().ToTable("T_ADMIN_PAGE_ROLE");
            builder.Entity<VodafoneOperation>().ToTable("T_ADMIN_OPERATION");

            builder.Entity<ApplicationUserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            builder.Entity<VodafonePageOperation>(menu_operation =>
            {
                menu_operation.HasKey(ur => new { ur.PageID, ur.OperationID });

                //menu_operation.HasOne(ur => ur.VodafoneMenuOperation)
                //    .WithMany(r => r.UserRoles)
                //    .HasForeignKey(ur => ur.RoleId)
                //    .IsRequired();

                //menu_operation.HasOne(ur => ur.User)
                //    .WithMany(r => r.UserRoles)
                //    .HasForeignKey(ur => ur.UserId)
                //    .IsRequired();
            });

            builder.Entity<VodafonePageRole>(menu_role =>
            {
                menu_role.HasKey(ur => new { ur.PageID, ur.RoleID });
            });
        }
    }
}
