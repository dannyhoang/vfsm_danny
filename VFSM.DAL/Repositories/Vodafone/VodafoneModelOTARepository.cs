﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Models.DBModels;

namespace VFSM.DAL.Repositories
{
    public class VodafoneModelOTARepository : Repository<VodafoneModelOTA>, IVodafoneModelOTARepository
    {
        public VodafoneModelOTARepository(VodafoneContext context) : base(context)
        { }

        private VodafoneContext _apContext => (VodafoneContext)_context;
    }
}
