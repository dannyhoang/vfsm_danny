﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Models.DBModels;

namespace VFSM.DAL.Repositories
{
    public interface IVodafoneDeviceRepository : IRepository<VodafoneDevice>
    {
    }
}
