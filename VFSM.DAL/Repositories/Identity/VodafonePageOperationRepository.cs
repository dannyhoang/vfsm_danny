﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Models;

namespace VFSM.DAL.Repositories
{
    public class VodafonePageOperationRepository : Repository<VodafonePageOperation>, IVodafonePageOperationRepository
    {
        public VodafonePageOperationRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _apContext => (ApplicationDbContext)_context;
    }
}
