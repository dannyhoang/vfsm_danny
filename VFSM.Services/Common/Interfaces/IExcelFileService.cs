﻿using System.Collections.Generic;

namespace VFSM.Services
{
    public interface IExcelFileService
    {
        // dynamic FillDataIntoTemplate(string fileName, System.Collections.Generic.IEnumerable<dynamic> listData, int headerRow, int dataStartRow);
        dynamic FillDataIntoTemplate(ExportFileViewModel dataModel);
        dynamic FillDataIntoTemplateMultiSheet(List<ExportFileViewModel> listDataModel, string fileName);
    }
}