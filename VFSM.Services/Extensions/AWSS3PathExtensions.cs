namespace VFSM.Services.Extensions
{
    public static class AWSS3PathExtensions
    {
        // 카테고리가 있을 경우 upload path 생성
        public static string GenerateAWSS3KeyWithCategory(string modelId, string version, string category, string fileName)
        {
            // Key = originalOta.ModelId + "/" + originalOta.Version + "/" + originalOta.ModelId + "_" + originalOta.Version + "_" + originalOta.FileName
            return string.Format("ota/{0}/{1}/{2}/{0}_{1}_{2}_{3}", modelId, version, category, fileName);
        }

        // 카테고리가 없을 경우 upload path 생성
        public static string GenerateAWSS3KeyWithoutCategory(string modelId, string version, string fileName)
        {
            return string.Format("ota/{0}/{1}/{0}_{1}_{2}", modelId, version, fileName);
        }

        // 카테고리가 있을 경우 삭제 path 생성
        public static string GenerateDeleteAWSS3KeyWithCategory(string modelId, string version, string category, string fileName)
        {
            // Key = originalOta.ModelId + "/" + originalOta.Version + "/" + originalOta.ModelId + "_" + originalOta.Version + "_" + originalOta.FileName
            return string.Format("ota/{0}/{1}/{2}/{3}", modelId, version, category, fileName);
        }

        // 카테고리가 없는 경우 삭제 path 생성
        public static string GenerateDeleteAWSS3KeyWithoutCategory(string modelId, string version, string fileName)
        {
            return string.Format("ota/{0}/{1}/{2}", modelId, version, fileName);
        }

        // 카테고리가 있을 경우 파일명 생성
        public static string GenerateFilenameWithCategory(string modelId, string version, string category, string fileName)
        {
            return string.Format("ota/{0}_{1}_{2}_{3}", modelId, version, category, fileName);
        }

        // 카테고리가 없을 경우 파일명 생성
        public static string GenerateFilenameWithoutCategory(string modelId, string version, string fileName)
        {
            return string.Format("{0}_{1}_{2}", modelId, version, fileName);
        }
    }
}