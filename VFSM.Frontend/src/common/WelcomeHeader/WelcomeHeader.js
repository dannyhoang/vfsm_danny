import React from 'react';
import logo from './Logo@2x.jpg';
import './styles.scss';

export default class WelcomeHeader extends React.Component{
    constructor(props){
        super(props);
    }

    render(){

        return(
            <div className="vfsm-login-page-header-wrapper" type="flex" justify="center">
                <div  className="vfsm-login-form-first-visit-title"> Welcom to </div>
                <div  className="sidebar-logo">
                        <img src={logo} alt='V-SOS' />
                </div>
            </div>
        )
    }
}