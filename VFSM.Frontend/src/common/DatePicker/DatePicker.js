import React, { Component } from 'react';
import './style.scss';
import ReactDatePicker from 'react-datepicker';
import IconSvg from '../icon/IconSvg'

class DatePicker extends Component {
    state = {
        focused : false
    }

    onFocus = e => {
        this.setState({focused : true})
    }

    onBlur = e => {
        this.setState({focused: false})
    }

    onIconClick = ()=> {
        const input = this.calendar.input;
        let focus = input === document.activeElement;
        if(focus){
            input.blur();
        }else {
            input.focus();
        }
    }

    render() {
        const { className, label, ...otherProps } = this.props;
        const { focused } = this.state;
        return (
            <div className={ 'custom-date-picker ' + (className || '') + (focused ? ' focused':'')}>
                <label>{label}</label>
                <ReactDatePicker
                        ref={(c) => this.calendar = c}
                        {...otherProps}
                        onFocus ={this.onFocus}
                        onBlur = {this.onBlur}
                        calendar={this.calendar}
                    />
                <IconSvg name="DatePicker" className="date-icon" onClick = {this.onIconClick}/>
            </div>
        );
    }

};

export default DatePicker;
