import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { NavLink, withRouter } from 'react-router-dom';
// import { matchPath } from 'react-router'
// import { observer } from 'mobx-react';
import IconSvg from '../../common/icon/IconSvg'
import './style.scss'
import { Menu } from 'antd';
import SubMenu from 'antd/lib/menu/SubMenu';
import logo from './Logo@2x.jpg'
@inject('menuRoleStore')
@observer
class Sidebar extends Component {
    constructor(props){
        super(props);
    }
    

    getFlattenData = () => {
        const result = [];
        const { routes } = this.props.menuRoleStore;
        routes.forEach(route => {
            if (route.operationValue !== '' && route.operationValue !== null ) {
                if (route.children) {
                    result.push({
                        name: route.name,
                        icon: route.icon,
                        path: route.path,
                        children: route.children.filter(child => child.operationValue !== ''),
                    });
                } else {
                    result.push({
                        name: route.name,
                        icon: route.icon,
                        path: route.path
                    });
                }
            }
        })

        return result;
    }

    fineDefaultPage = () => {
        const locationName = this.props.location.pathname;
        const routes  = this.getFlattenData();
        let index = routes.findIndex(o => o.path === locationName);
        if (index === -1) {
            for (let route of routes) {
                if (route.children) {
                    index = route.children.findIndex(child => child.path === locationName);
                    if (index !== -1) {
                        return 'child' + index + ' sub' + routes.findIndex(o => o === route);
                    }
                }
                else {
                    index = routes.findIndex(child => (locationName.indexOf(child.path) === 0 ));
                    if (index !== -1) {
                        return index;
                    }
                }
            }
        }
        return index;
    }

    isActive = (name) => {
        const currentPath = this.props.location.pathname;
        const  menuSideBar = this.getFlattenData();

        for (let g of menuSideBar) {
            if (!g.children) {
                if (g.path === currentPath) {
                    if (g.name === name) {
                        return true;
                    }
                }
            } else {
                for (let child of g.children) {
                    if (child.path === currentPath) {
                        if (g.name === name) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    getActiveClassName = (name) => {
        return this.isActive(name) ? ' active' : '';
    }

    render() {
        const data = this.getFlattenData();
        const selectedMenu = this.fineDefaultPage();
        let defaultSelected = selectedMenu === -1 ? 0 : selectedMenu;
        let defaultOpened = '';
        if (typeof(this.fineDefaultPage()) === 'string' && this.fineDefaultPage().indexOf('child') >= 0) {
            defaultOpened = this.fineDefaultPage().split(' ')[1];
            defaultSelected = this.fineDefaultPage().split(' ')[0];
        }
        return (
            <div className="sidebar">
                <div className="sidebar-logo">
                    <img src={logo} alt='' />
                </div>
                <div className="sidebar-menu">
                    <Menu defaultSelectedKeys={['' + defaultSelected]}  selectedKeys={['' + defaultSelected]} defaultOpenKeys={['' + defaultOpened]} mode="inline">
                        {
                            data.map((item, index) => {
                                return item.children ? (
                                    <SubMenu
                                        key={"sub" + index}
                                        title={<span><IconSvg name={item.icon} className={"sidebar-icon" + this.getActiveClassName(item.name)} /><span>{item.name}</span></span>}
                                    >
                                        {
                                            item.children.map((itemChild, indexChild) =>
                                                <Menu.Item key={"child" + indexChild}>
                                                    <NavLink
                                                        className='menu-item-children'
                                                        to={itemChild.path}
                                                        key={indexChild}>
                                                        {itemChild.name}
                                                    </NavLink>
                                                </Menu.Item>)
                                        }
                                    </SubMenu>) : (
                                        <Menu.Item key={index}>
                                            <IconSvg name={item.icon} className={"sidebar-icon" + this.getActiveClassName(item.name)} />
                                            <NavLink
                                                className='menu-item-root'
                                                to={item.path}
                                                key={index}>
                                                {item.name}
                                            </NavLink>
                                        </Menu.Item>
                                    )
                            })
                        }
                    </Menu>
                </div>
            </div>

        );
    }

};

export default withRouter(Sidebar);
