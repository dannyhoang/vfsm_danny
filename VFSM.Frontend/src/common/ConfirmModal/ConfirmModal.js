import React from 'react';
import { inject, observer } from 'mobx-react';
import { Modal, Button } from 'antd';
import './styles.scss';

@inject('apiStore')
@observer
class ConfirmModal extends React.Component{
    render(){
        const { visible, confirmDisable, title, handleConfirm, handleCloseModal, ...otherProps} = this.props;
        const { loading } = this.props.apiStore;

        return(
            <Modal
                visible={visible}
                footer={null}
                title={title}
                wrapClassName='modal-comfirm'
                onCancel={handleCloseModal}
            >
                <Button 
                disabled={confirmDisable} 
                type="primary" 
                onClick={handleConfirm} 
                loading={loading}
                {...otherProps}>Confirm</Button>
                <span className='body-cancel' onClick={handleCloseModal}>Cancel</span>
            </Modal>
        );
    };
};
export default ConfirmModal;