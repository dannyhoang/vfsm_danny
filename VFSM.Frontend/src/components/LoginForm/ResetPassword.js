import React from 'react';
import { inject, observer } from 'mobx-react';
import Cookies from 'js-cookie';
import { Form, Icon, Input, Button, Checkbox, Row, Col } from 'antd';
import  VodaButton  from '../../common/VodaButton/VodaButton';
import VodaInput from '../../common/Input/VodaInput';
import VodaCheckBox from '../../common/Input/VodaCheckBox';
import WelcomeHeader from '../../common/WelcomeHeader/WelcomeHeader';
import { validateEmail, validatePWLowerCase , validatePWUpperCase, validatePWAlpha , validatePWMinLen, validatePWDigit } from '../../core/utils/validate';
import Constant  from '../../core/utils/Constant';
// import css
import './styles.scss';
const { USER_ERROR, PASSWORD_ERROR, API_STATUS } = Constant;

@inject('rootStore')
@observer
class ResetPassword extends React.Component{
    constructor(props){
        super(props);
        this.state={
            emailInput: {
                value: '',
                isShowError: false,
                errorMsg: '',
                isShowGuidMsg: false,
                guidMsg: '',
            },
            passwordInput: {
                value: '',
                isShowError: false,
                errorMsg: '',
                isShowGuidMsg: false,
                guidMsg: '',
            },
            confirmPasswordInput: {
                value: '',
                isShowError: false,
                errorMsg: '',
                isShowGuidMsg: false,
                guidMsg: '',
            },
            isSusscess: false,
            code: (this.props.location.search.split('=')[1]) ? (this.props.location.search.split('=')[1]) : ''
        }
    }

    /* Check Input */
    checkEmailInput = () => {
        const { emailInput } = this.state;
        if(emailInput.value === ''){
            this.setState({
                emailInput: {
                    ...this.state.emailInput,
                    isShowError: true,
                    errorMsg: USER_ERROR.REQUIRED,
                },
            })
        } else {
            this.setState( () => ({
                emailInput: {
                    ...this.state.emailInput,
                    isShowError: false,
                    errorMsg: "",
                },
            }));
        }
    }

    checkPasswordInput = () => {
        const { passwordInput } = this.state;
        if(passwordInput.value === ''){
            this.setState({
                passwordInput: {
                    ...this.state.passwordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.REQUIRED,
                },
            })
        } else {
            this.setState( () => ({
                passwordInput: {
                    ...this.state.passwordInput,
                    isShowError: false,
                    errorMsg: "",
                },
            }));
        }
    }

    checkConfirmPasswordInput = () => {
        const { confirmPasswordInput } = this.state;
        if(confirmPasswordInput.value === ''){
            this.setState({
                confirmPasswordInput: {
                    ...this.state.confirmPasswordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.REQUIRED,
                },
            })
        }else {
            this.setState( () => ({
                confirmPasswordInput: {
                    ...this.state.confirmPasswordInput,
                    isShowError: false,
                    errorMsg: "",
                },
            }));
        }
    }

    /* handle  event onChange*/
    handleInputEmail = (e) => {
        this.setState({
            emailInput: {
                ...this.state.emailInput,
                value: e.target.value,
            },
        }, () => this.checkEmailInput());
    }

    handleInputPassword = (e) => {
        this.setState({
            passwordInput: {
                ...this.state.passwordInput,
                value: e.target.value,
            },
        }, () => this.checkPasswordInput());
    }

    handleInputConfirmPassword = (e) => {
        this.setState({
            confirmPasswordInput: {
                ...this.state.confirmPasswordInput,
                value: e.target.value,
            },
        }, () => this.checkConfirmPasswordInput());
    }

    /* other */

    checkIsSamePassword = () => {
        const { passwordInput, confirmPasswordInput } = this.state;
        if(passwordInput.value === confirmPasswordInput.value){
            return true;
        }
        return false;
    }

    checkValidatePassword = () => {
        const { passwordInput } = this.state;
        
        if( passwordInput.value.length < 6 ){
            this.setState({
                passwordInput: {
                    ...this.state.passwordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.MIN_LENGTH,
                },
            })
            return false;
        }
        else if( !validatePWLowerCase(passwordInput.value)){
            this.setState({
                passwordInput: {
                    ...this.state.passwordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.ONE_LOWER_CASE,
                },
            })
            return false;
        }
        else if( !validatePWUpperCase(passwordInput.value)){
            this.setState({
                passwordInput: {
                    ...this.state.passwordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.ONE_UPPER_CASE,
                },
            })
            return false;
        }
        else if( !validatePWDigit(passwordInput.value)){
            this.setState({
                passwordInput: {
                    ...this.state.passwordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.ONE_DIGIT,
                },
            })
            return false;
        }
        else if( !validatePWAlpha(passwordInput.value)){
            this.setState({
                passwordInput: {
                    ...this.state.passwordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.ONE_ALPHANUMERIC,
                },
            })
            return false;
        }
        return true;
    }

    checkInputValidation =() => {
        const { emailInput, passwordInput, confirmPasswordInput } = this.state;
        if( emailInput.isShowError || passwordInput.isShowError || confirmPasswordInput.isShowError){
            return false;
        }
        if(emailInput.value === '' || passwordInput.value === '' || confirmPasswordInput.value ===''){
            return false;
        }
        if(!this.checkIsSamePassword()){
            return false;
        }
        return true;
    }

    sendRequestCallBack = (msg) => {
        if( msg && msg.toUpperCase() === API_STATUS.SUCCESS){
            this.setState({
                isSusscess: true,
                emailInput: {
                    ...this.state.emailInput,
                    isShowError: false,
                    errorMsg: USER_ERROR.EMPTY,
                },
            })
        } else {
            this.setState({
                isSusscess: false,
                emailInput: {
                    ...this.state.emailInput,
                    isShowError: true,
                    errorMsg: USER_ERROR.INVALID,
                },
            }) 
        }
        // this.setState({
        //     isSusscess: true,
        // })
    }

    sendRequest = () => {
        const { emailInput, passwordInput, confirmPasswordInput, code } = this.state;
        const { rootStore } = this.props;
        if( this.checkInputValidation()){
            const body = {
                Email: emailInput.value,
                Password: passwordInput.value,
                ConfirmPassword: confirmPasswordInput.value,
                code: code,
            }
            rootStore.account.ResetPassword(body, this.sendRequestCallBack);
        }
    }

    handleOnClick = () => {
        const { emailInput, passwordInput, confirmPasswordInput } = this.state;
        if(emailInput.value === ''){
            this.setState({
                emailInput: {
                    ...this.state.emailInput,
                    isShowError: true,
                    errorMsg: USER_ERROR.REQUIRED,
                },
            })
        } else if ( !validateEmail (emailInput.value)){
            this.setState({
                emailInput: {
                    ...this.state.emailInput,
                    isShowError: true,
                    errorMsg: USER_ERROR.INCORRECT_FORMAT,
                },
            })
        } else {
            this.setState({
                emailInput: {
                    ...this.state.emailInput,
                    isShowError: false,
                    errorMsg: "",
                },
            }) 
        }

        if (passwordInput.value === ''){
            this.setState({
                passwordInput: {
                    ...this.state.passwordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.REQUIRED,
                },
            })
        } else {
            this.checkValidatePassword();
        }
        if(confirmPasswordInput.value === ''){
            this.setState({
                confirmPasswordInput: {
                    ...this.state.confirmPasswordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.REQUIRED,
                },
            }) 
        } else if (!this.checkIsSamePassword()){
            this.setState({
                confirmPasswordInput: {
                    ...this.state.confirmPasswordInput,
                    isShowError: true,
                    errorMsg: PASSWORD_ERROR.NOT_SAME,
                },

            }) 
        } else {
            this.setState({
                confirmPasswordInput: {
                    ...this.state.confirmPasswordInput,
                    isShowError: false,
                    errorMsg: "",
                },

            }) 
        }
        if (this.checkValidatePassword()) {
            this.sendRequest();
        }
    }

    handleOnLogin = () => {
        this.props.history.replace('/login');
    }
    
    render(){
        const { emailInput, passwordInput, confirmPasswordInput, isSusscess } = this.state;
        return(
           
            <div className="vfsm-register-wrapper">
                <WelcomeHeader />
                <div className="vfsm-register-header">
                    <div className="vfsm-register-title-wrapper">
                        <div className="vfsm-register-title">
                            <span style={{'font-weight': 'bold'}}>V-SOS </span>
                            <span>Band service management</span>
                        </div>
                        <div className="vfsm-register-gui">
                            {
                                !isSusscess ?   <span> Setup your password</span> : <span> Your password has been setup</span>
                            }
                        </div>
                    </div>
                </div>
                {
                !isSusscess ? 
                <div className="vfsm-register-container-wrapper">
                    <div className="vfsm-register-form-dash"></div>
                    <VodaInput 
                       // className="vfsm-fogotpassword-input" 
                        title="Email"
                        width={100}
                        height={50}
                        placeholder="Type" 
                        onChange={this.handleInputEmail}
                        onPressEnter={this.handleOnClick}
                        value={emailInput.value}
                        options={{
                            isShowError:emailInput.isShowError,
                            errorMsg: emailInput.errorMsg,
                            isShowGuidMsg: emailInput.ShowGuidMsg,
                            guidMsg : emailInput.guidMsg,
                        }}
                    />
                    <VodaInput 
                     //   className="vfsm-fogotpassword-input" 
                        title="Password"
                        type="password"
                        width={100}
                        height={50}
                        placeholder="Type" 
                        onChange={this.handleInputPassword}
                        onPressEnter={this.handleOnClick}
                        value={passwordInput.value}
                        options={{
                            isShowError: passwordInput.isShowError,
                            errorMsg: passwordInput.errorMsg,
                            isShowGuidMsg: passwordInput.isShowGuidMsg,
                            guidMsg : passwordInput.guidMsg,
                        }}
                    />
                    <VodaInput 
                     //   className="vfsm-fogotpassword-input" 
                        title="Confirm Password"
                        type="password"
                        width={100}
                        height={50}
                        placeholder="Type" 
                        onChange={this.handleInputConfirmPassword}
                        onPressEnter={this.handleOnClick}
                        value={confirmPasswordInput.value}
                        options={{
                            isShowError: confirmPasswordInput.isShowError,
                            errorMsg: confirmPasswordInput.errorMsg,
                            isShowGuidMsg: confirmPasswordInput.isShowGuidMsg,
                            guidMsg : confirmPasswordInput.guidMsg,
                        }}
                    />
                    <div className="vfsm-register-form-button-box">
                        <VodaButton 
                            width={"100%"}
                            text="Confirm" 
                            onClick={this.handleOnClick} 
                        />
                    </div>
                </div>
                : 
                <div className="vfsm-register-container-wrapper">
                    <div className="vfsm-register-form-dash"></div>
                    <div className="vfsm-register-login-button">
                        <VodaButton 
                            width={"100%"}
                            text="Log in" 
                            onClick={this.handleOnLogin} 
                        />
                    </div>
                </div>
                }
                {/* <div className="vfsm-page-footer-wrapper">
                    <span className="vfsm-page-footer-contact">
                        Contact
                    </span>
                    <span className="vfsm-page-footer-humax">
                        Copyright © 2018 HUMAX Co., Ltd. All rights reserved.
                    </span>
                </div> */}
            </div>
        )
    }
}

export default ResetPassword;