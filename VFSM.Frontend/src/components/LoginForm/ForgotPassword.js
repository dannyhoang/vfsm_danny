import React from 'react';
import { inject, observer } from 'mobx-react';
import Cookies from 'js-cookie';
import { Form, Icon, Input, Button, Checkbox, Row, Col } from 'antd';
import  VodaButton  from '../../common/VodaButton/VodaButton';
import VodaInput from '../../common/Input/VodaInput';
import VodaCheckBox from '../../common/Input/VodaCheckBox';
import { validateEmail } from '../../core/utils/validate';
import WelcomeHeader from '../../common/WelcomeHeader/WelcomeHeader';
import Constant  from '../../../src/core/utils/Constant';
// import css
import './styles.scss';

const { EMAIL_GUIDMSG, EMAIL_ERROR } = Constant;
@inject('rootStore')
@observer
class ForgotPassword extends React.Component{

    constructor(props){
        super(props);
        this.state={
            isShowError: false,
            errorMsg: '',
            isShowGuidMsg: false,
            guidMsg: '',
            emailInput: ''
            
        }
    }

    componentDidMount(){

    }

    /* Call Back*/

    forgotPasswordCallBack = (msg) => {
        if(msg.toUpperCase() === 'SUCCESS'){
            this.setState({
                isShowError: false,
                isShowGuidMsg: true,
                guidMsg: EMAIL_GUIDMSG.CONFIRM_SUSSESS ,
            });
       // this.props.history.push(`/login`);
        } else {
            this.setState({
                isShowError: true,
                isShowGuidMsg: false,
                errorMsg: EMAIL_ERROR.NOT_FOUND,
            })
        }
    }

    /* OnClick */
    handleOnClick = () => {
        const { emailInput, isShowError, isShowGuidMsg} = this.state;
        const { rootStore } = this.props;
        if(emailInput === ''){
            this.setState({
                isShowError: true,
                errorMsg: 'This field is required'
            })
        } else if (emailInput !== '' && validateEmail(emailInput)){
            const payload = {
                email: emailInput,
            }
            rootStore.account.forgotPassword(payload, this.forgotPasswordCallBack);
        } else {
            this.setState({
                isShowError: true,
                isShowGuidMsg: false,
                errorMsg: 'User Not Found.',
            })
        }

    }
    /* Other */
    checkEmailInput = () => {
        const { emailInput, } = this.state;


    }
    /* OnChange */

    handleInputEmail = (e) => {
        this.setState({
            emailInput: e.target.value,
            isShowError: false,
            errorMsg: ''
        });
    }

    render(){
        const { isShowError, isShowGuidMsg, guidMsg,  errorMsg} = this.state;

        return(
            <div className="vfsm-fogotpassword-page-wrapper">
                < WelcomeHeader />
                <div className="vfsm-fogotpassword-page-container-wrapper">
                {
                    <div className="vfsm-fogotpassword-form-wrapper"> 
                            <div className="vfsm-fogotpassword-form-first-visit-content">
                                <span style={{'font-weight': 'bold'}}>Reset your password </span>
                            </div>
                            <div className="vfsm-fogotpassword-form-guid">
                                <span> Enter the email address below and we’ll email you </span>
                                <span> a link to reset your password</span>
                            </div>
                            <div className="vfsm-fogotpassword-form-dash"></div>
                            <div className="vfsm-fogotpassword-form">
                                <VodaInput 
                                    className="vfsm-fogotpassword-input" 
                                    title="Email"
                                    width={100}
                                    height={50}
                                    placeholder="Type" 
                                    onChange={this.handleInputEmail} 
                                    value={this.state.emailInput}
                                    options={{
                                        isShowError:isShowError,
                                        errorMsg: errorMsg,
                                        isShowGuidMsg:isShowGuidMsg,
                                        guidMsg : guidMsg,
                                    }}
                                />
                                <div className="vfsm-fogotpassword-form-button-box">
                                    <VodaButton 
                                        text="Send reset email" 
                                        onClick={this.handleOnClick} 
                                    />
                                </div>
                            </div>
                    </div>
                }
                </div>
                {/* <div className="vfsm-page-footer-wrapper">
                    <span className="vfsm-page-footer-contact">
                        Contact
                    </span>
                    <span className="vfsm-page-footer-humax">
                        Copyright © 2018 HUMAX Co., Ltd. All rights reserved.
                    </span>
                </div> */}
            </div>
        )
    }
}

export default ForgotPassword;