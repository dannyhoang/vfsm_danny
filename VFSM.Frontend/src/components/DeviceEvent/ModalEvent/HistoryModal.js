
import React from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import ModalEvent from './ModalEvent';
import { DateTime } from '../../../core/utils';

const { formatTime, formatTimeToSend } = DateTime;
@inject('rootStore')
@observer
class HistoryModal extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // Get History detail
        const { deviceId, rootStore } = this.props;
        rootStore.event.getDetail(deviceId, { type: 'HISTORY' });
    }

    render() {

        const dataDetail = this.props.rootStore.event.eventDetail;
        
        const columns = [{
            title: 'Created Date',
            dataIndex: 'createdDate',
            width:'20%',
            sorter: (a, b) => new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime()
        }, {
            title: 'Activity ID',
            dataIndex: 'activityId', 
            width:'20%',
        }, {
            title: 'Event Type',
            dataIndex: 'eventType',
            width:'15%'
        }, {
            title: 'Location Source',
            dataIndex: 'locationSource',
            width:'15%'
        }];

        const data = [];

        dataDetail && dataDetail.map((obj, index)=> {
            data.push({
                key: index,
                createdDate: formatTime(obj.createdDate),
                eventType: obj.eventType,
                activityId: obj.activityId,
                accuracy: obj.accuracy,
                locationSource: obj.locSource,
                lat: obj.lat,
                lng: obj.lng,
                latBeacon: obj.latBeacon,
                lngBeacon: obj.lngBeacon
            });
        });
        return (

            <ModalEvent columns={columns} data={data} typeModal="vfsm-modal-event-history" haveSeries={false}/>
        )
    }

}

HistoryModal.propTypes = {
    deviceId: PropTypes.string.isRequired
};
export default HistoryModal;