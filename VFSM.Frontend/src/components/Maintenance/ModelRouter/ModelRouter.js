import OTA from "../OTA/OTA";
import MaintenanceContainer from "../MaintenanceContainer/MaintenanceContainer";


const ModelRouter = [
    {
        name: 'ModelOTA',
        path: 'OTA',
        component: OTA,
        exact: false
    },
    {
        name: 'Maintenance',
        path: '',
        component: MaintenanceContainer,
        exact: true
    },

]

export default ModelRouter;