
import React from 'react';
import { toJS } from "mobx";
import { inject, observer } from 'mobx-react';
import { Row, Col, Popconfirm } from 'antd';
import EditableTable from './EditableTable/EditableTable';
import VodaButton from 'common/VodaButton/VodaButton';
import columns from 'core/utils/ColumsTable';
import {DateTime } from '../../../core/utils';
import './styles.scss';

const { formatTimeDetail } = DateTime ;
const { columnsMC60, columnsBracelet } = columns;
@inject('rootStore')
@inject('menuRoleStore')
@observer
class OTA extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            category: 'MC60',
            isDisabledAdd : false,
            modelId: (this.props.location.search.split('=')[1]) ? (this.props.location.search.split('=')[1]) : ''
        };
        
        this.props.rootStore.ota.setModelId(this.state.modelId);
        this.data = '';
        
        this.styleActive = {
            backgroundColor: '#B23737',
            height: '63px',
            color: '#F4F4F4',
            fontFamily: 'Vodafone',
            fontSize: '20px',
            fontWeight: 'bold',
            textAlign: 'center',
            paddingTop: '18px',
            borderTop: '1px solid #E3E3E3',
        }

        this.normalstyle = {
            height: '63px',
            color: '#4B4D4E',
            fontFamily: 'Vodafone',
            fontSize: '20px',
            fontWeight: 'bold',
            textAlign: 'center',
            paddingTop: '18px',
            borderTop: '1px solid #E3E3E3',
        }
    }
    componentDidMount() {
        this.props.rootStore.ota.getByModelId(this.state.modelId);
    }

    onClickChooseOption = (e) => {
        this.setState({ 
            category: e , 
            isDisabledAdd: false
        });
    }

    handleBackToModels = () => {
        this.props.history.push(`/main/Maintenance`);
        this.resetData();
    }
    resetData = () => {
        this.props.rootStore.ota.resetData();
    }
    handleClickEditOrDelete = (eventName, item) => {
        if (eventName === 'Detail') {
            var a = item;
            // this.handleOpenModelDetailModal(id);
        }
        else if (eventName === 'Delete') {
            // this.handleDeleteModel(id);
        }
    }
    addNew = () => {
        this.addNewRecord.handleClickAdd();
    }
    deleteRecord = (param, callback) => {
        if (param != null) {
            let deleteList = [];
            deleteList.push(param);
            this.props.rootStore.ota.deleteOtaList(deleteList, callback);
        }
    }

    checkDisableAddButton = ( isDisabled ) => {
        this.setState({isDisabledAdd: isDisabled});
    }

    render() {
        const { isDisabledAdd, modelId } = this.state;
        const { menuRoles } = this.props.menuRoleStore;
            return (
                <div className="model-detail-wapper">
                    <div className="model-detail-header">
                        {
                            menuRoles && menuRoles.maintenance == 'operation.write' ? (
                            <div className="model-detail-add-new">
                                <VodaButton text="Add new" onClick={this.addNew} disabled={isDisabledAdd}/>
                            </div>) : null
                        }
                        <div className="model-detail-header-button">
                            <VodaButton text="Back To model" onClick={this.handleBackToModels} />
                        </div>
                    </div>
                    <div className="model-detail-container">
                        <div className="model-detail-title">
                            <div className="model-detail-title-constant">
                                OTA List [Model ID : {modelId}]
                            </div>
                            <div className="model-detail-title-dash" />
                        </div>
                        <div className="model-detail-button-option">
                            <div className="model-detail-button-option-action">
                                <Row>
                                    <Col
                                        style={this.state.category === 'MC60' ? this.styleActive : this.normalstyle}
                                        span={12}
                                        onClick={e => { this.onClickChooseOption('MC60') }}
                                    >
                                        MC60
                                    </Col>
                                    <Col
                                        style={this.state.category === 'FIRMWARE' ? this.styleActive : this.normalstyle}
                                        span={12}
                                        onClick={e => { this.onClickChooseOption('FIRMWARE') }}
                                    >
                                        BRACELET
                                    </Col>
                                </Row>
                            </div>
                            <div className="model-detail-button-option-footer" />
                        </div>
                        {
                            menuRoles.maintenance 
                            && <div>
                                <div className="model-detail-constant" id="model-detail-constant-MC60">
                                    {
                                        this.state.category === 'MC60'
                                        && <EditableTable
                                            category={this.state.category}
                                            columns={columnsMC60}
                                            //save={this.saveEditRecord}
                                            onRef={ref => (this.addNewRecord = ref)} 
                                            deleteRecord={this.deleteRecord} 
                                            acceptFileUpload='.img'
                                            menuRole={menuRoles.maintenance}
                                            checkDisableAddButton={this.checkDisableAddButton}
                                        />
                                    }

                                </div>
                                <div className="model-detail-constant" id="model-detail-constant-bracelet">
                                    {
                                        this.state.category === 'FIRMWARE'
                                        && <EditableTable
                                            category={this.state.category}
                                            columns={columnsBracelet}
                                            //save={this.saveEditRecord}
                                            onRef={ref => (this.addNewRecord = ref)}
                                            deleteRecord={this.deleteRecord}
                                            acceptFileUpload='.img' 
                                            menuRole={menuRoles.maintenance}
                                            checkDisableAddButton={this.checkDisableAddButton}
                                        />
                                    }
                                </div>
                            </div>
                        }
                    </div>
                </div>
            )
    }
}

export default OTA;