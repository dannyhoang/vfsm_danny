import React from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import Device from './Device/Device';
import { Modal } from 'antd';
import VodaTable from '../../../common/VodaTable/VodaTable';
import VodaButton from '../../../common/VodaButton/VodaButton';
import DeviceGoogleMap from '../../../common/GoogleMap/DeviceGoogleMap';
import './styles.scss'

@inject('rootStore')
@observer
class SeeMoreModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showMap: false,
            latDevice: null,
            lngDevice: null,
            latBeacon: null,
            lngBeacon: null,
            openDevice: false,
            deviceId: undefined,
        }
    }

    // Show device modal
    handleShowDevice = (device) => {
        this.setState({ openDevice: true, device: device });
    }

    showGoogleMap = (e) => {
        // Get device detail
        this.props.rootStore.device.getDetail(e.deviceId);
        // this.setState({
        //     showMap: true,
        //     latDevice: e.lat,
        //     lngDevice: e.lng,
        //     latBeacon: e.latBeacon,
        //     lngBeacon: e.lngBeacon,
        //     deviceId: e.deviceId
        // });
        this.setState({
            showMap: true,
            deviceId: e.deviceId
        });
    }

    handleCloseMap = () => {
        this.setState({
            showMap: false,
            latDevice: null,
            lngDevice: null,
            latBeacon: null,
            lngBeacon: null
        });
    }

    renderTable = () => {
        const { data } = this.props;
        const dataNew = data.slice();
        let columnNew = [{
            title: 'Activated Date',
            dataIndex: 'activatedDate',
            width:'20%',
            sorter: (a, b) => new Date(b.activatedDate).getTime() - new Date(a.activatedDate).getTime()
        }, {
            title: 'Device ID',
            dataIndex: 'deviceId', 
            width:'20%',
        }, {
            title: 'Imei',
            dataIndex: 'imei',
            width:'15%'
        }, {
            title: 'Version',
            dataIndex: 'deviceVersion',
            width:'15%'
        }, {
            title: 'Role',
            dataIndex: 'role',
            width:'10%'
        }, {
            title: 'Status',
            dataIndex: 'status',
            width:'10%'
        }, {
            title: 'Location',
            dataIndex: 'location',
            width:'10%'
        }];
        dataNew.map((e, index) => {
            e.location = <VodaButton className="device-list-map-button" size="small" onClick={() => this.showGoogleMap(e)} text="See map" iconType="Location" ></VodaButton>;
            return e;
        });
        return <VodaTable columns={columnNew} data={dataNew} scroll={{ y: 460 }} />;
    }

    renderMap = () => {
        const { deviceId, showMap } = this.state;
        const dataDetail = this.props.rootStore.device.deviceDetail;
        const datalocation = [];
        if (dataDetail.lat !== null && dataDetail.lat !== undefined  && dataDetail.lng !== null && dataDetail.lng  !== undefined ) {
            datalocation.push(
                {
                    title: 'Device',
                    location: {
                        lat: dataDetail.lat ,
                        lng: dataDetail.lng ,
                    }
                }
            )
        }
        if (dataDetail.latBeacon !== null && dataDetail.latBeacon !== undefined && dataDetail.lngBeacon !== null && dataDetail.lngBeacon !== undefined) {
            datalocation.push(
                {
                    title: 'Beacon',
                    location: {
                        lat: dataDetail.latBeacon,
                        lng: dataDetail.lngBeacon,
                    },
                    beaconMac: dataDetail.beaconMac ? dataDetail.beaconMac : '--'
                }
            )
        }
        return datalocation.length > 0 
            ? 
            (
                <Modal
                    width={1200}
                    footer={null}
                    title={"Device ID : " + deviceId}
                    visible={showMap}
                    onCancel={this.handleCloseMap}
                    className="see-more-modal"
                >
                    <DeviceGoogleMap data={datalocation} />
                </Modal>
            )
            : (
                <Modal
                    width={1200}
                    footer={null}
                    title={"Device ID : " + deviceId}
                    visible={showMap}
                    onCancel={this.handleCloseMap}
                    className="see-more-modal"
                >
                    <div style={{ fontSize: '14px', fontWeight: 200, padding: '50px 210px', fontStyle: 'italic' }}>This device have not location</div>
                </Modal>
            )
    }

    render() {

        return (
            <div>
                { this.renderTable() }
                { this.state.showMap ? this.renderMap(): '' }
            </div>
        )
    }
}

Device.PropTypes = {
    data: PropTypes.array.isRequired
}

export default SeeMoreModal;