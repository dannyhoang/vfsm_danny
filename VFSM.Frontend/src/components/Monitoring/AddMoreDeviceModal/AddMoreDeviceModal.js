
import React from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { Col, Input } from 'antd';
import Select from '../../../common/Select/Select';
import './styles.scss'
import ButtonFooter from '../../../common/ButtonFooter/ButtonFooter';
import ColModal from '../../../common/ColModal/ColModal';

@inject('rootStore')
@observer
class AddMoreDeviceModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            role: undefined,
            uid: undefined,
            disabled: true
        };
        this.option = [
            {
                label: 'CARETAKER',
                value: 'ROOT'
            },
            {
                label: 'INVITEE',
                value: 'SUB'
            }
        ];
    }

    checkDisabled = () => {
        const { role, uid } = this.state;
        role && uid && uid !== ''
            ? this.setState({ disabled: false })
            : this.setState({ disabled: true });
    }

    handleChangeRole = (value) => {
        this.setState({ role: value }, this.checkDisabled);
    }

    handleChangeUid = (e) => {
        this.setState({ uid: e.target.value }, this.checkDisabled);
    }

    handleAddDevice = () => {
        const { role, name, uid } = this.state;
        const dataBody = {
            userId: this.props.userId,
            role: role,
            deviceUid: uid
        }
        this.props.rootStore.user.addNewDevice(dataBody, this.callbackFunction);
    }

    callbackFunction = () => {
        this.props.onClose();
    }

    handleCancel = () => {
        this.props.onClose();
    }

    render() {
        const data = [
            {
                title: 'Role',
                content: <Select className="add-device-select" placeholder="Select" option={this.option} onChange={this.handleChangeRole} />,
                hasInput: true
            },
            {
                title: 'IMEI',
                content: <Input className="add-device-input" placeholder="Type" onChange={this.handleChangeUid} />,
                hasInput: true
            }
        ];

        return (
            <div className="add-modal">
                <Col>
                    {
                        data.map(obj => {
                            return (
                                <ColModal data={obj} />
                            )
                        })
                    }
                </Col>
                <ButtonFooter text="Add" disabled={this.state.disabled} handleClickFirst={this.handleAddDevice} handleClickCancel={this.handleCancel}/>
            </div>
        )
    }

}

AddMoreDeviceModal.PropTypes = {
    userId: PropTypes.string.isRequired
}

export default AddMoreDeviceModal;