import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import App from './App';
import LoginForm from '../src/components/LoginForm/LoginForm';
import ResetPassword from '../src/components/LoginForm/ResetPassword';
import ForgotPassword from '../src/components/LoginForm/ForgotPassword';
import Main from './components/Main/Main';
import RootStore from './stores/rootStore'
import { ApiStore, MenuRoleStore } from 'core/stores/index';
import registerServiceWorker from './registerServiceWorker';

const history = createBrowserHistory();
const rootStore = new RootStore();
ReactDOM.render(
    <Provider rootStore = {rootStore} apiStore={ApiStore} menuRoleStore={MenuRoleStore} >
        <Router
            history={history}
        >
            <Switch>
                <Route exact path='/login' component={LoginForm} />
                <Route extac path='/password/requestReset' component={ForgotPassword} />
                <Route exact path='/ResetPassword' component={ResetPassword} />
                <Route path="/" component={App} />
                <Route path="/main" component={App} />
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);



registerServiceWorker();
