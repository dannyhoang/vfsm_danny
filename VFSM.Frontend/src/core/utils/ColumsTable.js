import React from 'react';
import { Switch } from 'antd';

const columnsMC60 = [{
    title: 'S/W version',
    dataIndex: 'version',
    key: 'version',
    width: '15%',
    editable: true,
    type: 'text'
}, {
    title: 'Mapping Version',
    dataIndex: 'mappingVersion',
    key: 'mappingVersion',
    width: '15%',
    editable: true,
    type: 'text'
}, {
    title: 'Activation',
    dataIndex: 'isActive',
    key: 'isActive',
    render: ia => <Switch disabled={true} checked={ia} />,
    width: '15%',
    editable: true,
    type: 'switch'
}, {
    title: 'File Size (Byte)',
    dataIndex: 'fileSize',
    key: 'fileSize',
    width: '15%',
    editable: false,
}, {
    title: 'Uploaded Date',
    dataIndex: 'modifiedDate',
    key: 'modifiedDate',
    width: '15%',
    className: 'vfsm-ota-table-uploaded-date',
    editable: false,
}, {
    title: 'URL',
    dataIndex: 'otaUrl',
    key: 'otaUrl',
    render: url => <a href={url} target='_blank'>Get Link</a>,
    width: '17%',
    editable: true,
    type: 'file'
},
];

const columnsBracelet = [{
    title: 'S/W version',
    dataIndex: 'version',
    key: 'version',
    width: '18%',
    editable: true,
    type: 'text'
}, {
    title: 'Activation',
    dataIndex: 'isActive',
    key: 'isActive',
    render: ia => <Switch disabled={true} checked={ia} />,
    width: '18%',
    editable: true,
    type: 'switch'
}, {
    title: 'File Size (Byte)',
    dataIndex: 'fileSize',
    key: 'fileSize',
    width: '18%',
    editable: false,
}, {
    title: 'Uploaded Date',
    dataIndex: 'modifiedDate',
    key: 'modifiedDate',
    width: '18%',
    className: 'vfsm-ota-table-uploaded-date',
    editable: false,
}, {
    title: 'URL',
    dataIndex: 'otaUrl',
    key: 'otaUrl',
    render: url => <a href={url} target='_blank'>Get Link</a>,
    width: '20%',
    editable: true,
    type: 'file'
}
];

export default {
    columnsMC60 ,
    columnsBracelet
}
