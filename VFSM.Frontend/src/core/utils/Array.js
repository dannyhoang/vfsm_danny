import StringUtils from './String';
const { s } = StringUtils;

//accept observable array
const arr = (array) => {

    const getValue = (obj, field) => {
        if (!obj) return null;

        if (typeof field === 'string') {
            if (!field) {
                return obj.toString();
            }
            return obj[field];
        }


        return field(obj);
    }

    return {
        lookup(field, searchText) {

            if (!array || !array.map) {
                return [];
            }

            return array.map(item => {
                if (Array.isArray(field)) {
                    let maxPriority = 0;

                    field.forEach(child => {
                        let value = getValue(item, child);
                        let priority;
                        if (isNaN(value)) {
                            priority = s(value).hasWord(searchText);
                        } else {
                            priority = value === searchText;
                        }
                        maxPriority = Math.max(priority, maxPriority);

                    });
                    return {
                        item,
                        priority: maxPriority
                    }
                } else {

                    const value = getValue(item, field);
                    if (!isNaN(value)) {
                        return {
                            item,
                            priority: value === searchText
                        }
                    }
                    return {
                        item,
                        priority: s(value).hasWord(searchText)
                    }
                }
            }).filter(item => item.priority)
                .sort((a, b) => {
                    return a.priority - b.priority
                }).map(item => item.item)
        },
    }
}

export default {
    arr
}

