import { observable, action, configure, runInAction } from 'mobx';
import { DialogService } from 'core/services'

// don't allow state modifications outside actions
configure({
	enforceActions: 'observed'
});
class ApiStore {

    @observable loading = false;

    @action
    setLoading(loading) {
        this.loading = loading;
    }

}

export default new ApiStore();