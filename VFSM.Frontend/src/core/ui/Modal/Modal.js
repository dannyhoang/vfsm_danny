import React from 'react';
import { Modal as AntModal } from 'antd';

export default class Modal extends React.Component {
    render(){
        const { className, ...otherProps } = this.props;
        return(
            <AntModal
                className={'custom-modal ' + (className || '')}
                footer={null}
                closable={false} 
                {...otherProps}
            >{this.props.children}</AntModal>
        )
    }
};