import React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import './style.scss'
import DialogService from '../../services/DialogService';

const DialogBody = (props) => {
    const { children, className, ...otherProps } = props;
    return (
        <PerfectScrollbar suppressScrollX={true}>
            <div className={'dialog-body ' + (className || '')}  { ...otherProps }>{children}</div>
        </PerfectScrollbar>
    )
}

const DialogHeader = (props) => {

    const close = ()=> {
        if(props.onClose) {
            props.onClose();
        } else {
            DialogService.close();
        }
    }
    return (
        <div className='dialog-header'>
            <div className='dialog-title'>{props.children}</div>
            <span className="icon-ico_cancel" onClick={close}></span>
        </div>
    )
}

const DialogFooter = (props) => {
    return (
        <div className='dialog-footer'>{props.children}</div>
    )
}



export {
    DialogHeader,
    DialogBody,
    DialogFooter
}