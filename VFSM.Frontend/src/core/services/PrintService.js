



export default {

    print: (elem) => {
        var w = window.open('', 'PRINT', 'height=800,width=1000');

        fetch('http://localhost:4200/static/css/main.css')
            .then(response => response.text())
            .then((text) => {
                w.document.write(`
                    <html>
                        <head>
                            <style>
                                ${text}
        
                            </style>
                        </head>
                        <body>
                            ${elem.outerHTML}
                        </body>
                    </html>
                `)
                w.document.close(); // necessary for IE >= 10
                w.focus(); // necessary for IE >= 10*/
        
                w.print();
                w.close();
        
                return true;
            })
    }


}