'use strict';
import { ApiService } from '../core/services';

class EventService extends ApiService {

	constructor() {
		super('Activity');
	}

	getAll(body) {
		return this.get('', body);
	}

	getDetail(deviceId, body) {
		return this.get('GetByDeviceId/' + deviceId, body);
	}

	getLastActDetail(deviceId) {
		return this.get('LastAct/' + deviceId);
	}

}

export default new EventService();