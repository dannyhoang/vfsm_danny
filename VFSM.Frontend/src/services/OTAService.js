'use strict';

import { ApiService } from '../core/services';

class OTAService extends ApiService {

    constructor(){
        super('ota');
    }

    getByModelId(modelId) {
        return this.get('GetByModelId/'+modelId);
    }

    createOta(param) {
        return this.post('', param);
    }
    editOta(param) {
        return this.put('', param);
    }
    deleteOtaList(param) {
        return this.delete('', param);
    }

}

export default new OTAService();